N = 51
for i in range(N):
    amp = round(100 * (0.9 - 0.1 * (-0.8) ** (i + 3)), 3)
    percent = round(100 * (i / (N - 1)), 3)
    print(f"{percent}% {{ width: {amp}%; }}")
