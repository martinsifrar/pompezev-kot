% !TEX program = xelatex

\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{lit.bib}

% Images and hyperlinks
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathrsfs}

% Custom definitions
% ...
% "Defined as" symbol
\usepackage{mathtools}
\newcommand{\das}{\vcentcolon=}
\newcommand{\asd}{=\vcentcolon}
\newcommand{\Ell}{\mathcal{L}}
% Differential
\newcommand{\diff}{\mathrm{d}}
% Listings
\usepackage{listings}
\usepackage{xcolor}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    numberstyle=\tiny\color{codegray},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    xleftmargin=3em,
    aboveskip=1.5em,
    belowskip=1.5em,
    abovecaptionskip=1em
}
\lstset{style=mystyle}
\renewcommand{\lstlistingname}{Izsek}

\title{
\sc\large Modelska analiza I\\
\bigskip
\bf\Large 1.~naloga: Model vožnje skozi semafor: variacijska metoda
}
\author{Martin Šifrar, 28242021}

\begin{document}
\maketitle

\section{Navodila}

Varčno vožnjo lahko definiramo s pogojem, da je pospeševanja in zaviranja čim manj. To lahko dosežemo z minimizacijo kurulativnega kvadrata pospeška. Isčemo optimalni režim vožnje v situaciji, ko poskušamo razdaljo do semaforja prevoziti ravno v trenutku, ko se prižge zelena luč.

\section{Rešitev}

Vožnjo začnemo s hitrostjo $v_0$, na razdalji $L$ od semaforja. Iz izkušenj vemo, da se bo semafor prižgal v času $T$. Ker želimo semafor prevoziti točno ob času $T$, velja vez
\begin{equation}
    L = \int_0^T v(t) \,\diff t.
    \label{eq:vez}
\end{equation}
V namen udobne vožnje bomo minimizirali komulativni kvadrat pospeška $\int \dot{v}^2 \,\diff t$. Z upoštevanjem vezi to pomeni minimizacijo z Lagrangianom
\begin{equation}
    \Ell(v(t), \dot{v}(t)) \das \dot{v}^2 - \lambda v,
    \label{eq:orig-L}
\end{equation}
tako da minimiziramo akcijo
\begin{equation*}
    S_\lambda = \int_0^T \dot{v}^2 \,\diff t + \int_0^T v \,\diff t.
\end{equation*}
Nalogo sem reševal s pomočjo Mathematica programskega okolja, to vljučuje reševanje enačb in risanje grafov.

\subsection{Brezdimenzijska oblika}

Za lažje reševanje problema vpeljemo brezdimenzijske količine
\begin{equation}
    \tau \das \frac{t}{T}, \quad u \das \frac{v}{v_0}, \quad \eta \das \frac{L}{v_0 T},
    \label{eq:nondim}
\end{equation}
pri čemer ke $v_0 T$ nekakšna naravna hitrost, saj pri $L = v_0 T$ uvidimo trivialno optimalno rešitev v nepospešeni vožnji s konstantno hitrostjo (tako da je akcija kar $S = 0$). V takih brezdimenzijskih koordinatah se Lagrangian glasi
\begin{equation*}
    \Ell'(u(\tau), \dot{u}(\tau)) \das \frac{v_0^2}{T^2} \left[ \dot{u}^2 - \left( \frac{T^2}{v_0} \lambda \right) u \right],
\end{equation*}
pri čemer so pike nad črko zdaj odvodi $\frac{\diff}{\diff \tau}$ namesto $\frac{\diff}{\diff t}$. Lagrangian lahko še nekoliko reskalirano
\begin{equation}
    \Ell(u(\tau), \dot{u}(\tau)) \das \dot{u}^2 - \left( \frac{T}{v_0^2} \lambda \right) u = \dot{u}^2 - \widetilde{\lambda} u,
    \label{eq:L}
\end{equation}
pri čemer je naš novi multiplikator $\widetilde{\lambda} \das \frac{T^2}{v_0} \lambda$. Če za Lagrangian~(\ref{eq:L}) zapišemo Euler-Lagrange enačbe, dobimo s kratkim računom navadno diferencialno enačbo
\begin{equation}
    \ddot{u} = -\frac{\widetilde{\lambda}}{2}, \quad \tau \in [0, 1],
    \label{eq:ODE1}
\end{equation}
z začetnim pogojem $u(0) = 1$.

\subsection{Extremalna hitrost pri semaforju}

Semafor želimo prevoziti z ekstremalno hitrostjo, tako da velja $\dot{u}(T) = 0$. Če rešimo navadno diferencialno enačbo~\ref{eq:ODE1} in apliciramo pogoja
\begin{equation*}
    \text{odprta robna pogoja: } u(0) = 1, \quad \dot{u}(1) = 0,
\end{equation*}
dobimo za rešitev družino krivulj
\begin{equation*}
    u(\tau) = \frac{1}{4} \left( 4 + 2\widetilde{\lambda} t - \widetilde{\lambda} t^2 \right),
\end{equation*}
izmed katerih pa je ustrezna samo tista, ki ustreza vezi~(\ref{eq:vez}). S preprosto integracijo in obratom dobimo izraz za multiplikator $\widetilde{\lambda} = 6(\eta - 1)$, s čimer zapišemo rešitev kar po parametru $\eta$ kot
\begin{equation}
    u(\tau) = 1 + (\eta - 1) \left( 3\tau - \frac{3\tau^2}{2} \right).
    \label{eq:open-u}
\end{equation}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.8\textwidth]{closed.png}
    \end{center}
    \caption{Rešitve~\ref{eq:open-w} za hitrost (levo) in pot (desno) pri določeni hitrosti $w(1) = w_1$ (zaprt robni pogoj). Imamo dva prosta parametra $w_0$, $w_1$. Spet opazimo nekaj primerov, kjer \textit{overshoot}-amo semafor, kar naslovimo posebej v poglavju~\ref{sec:overshoots}.}
    \label{fig:closed}
\end{figure}

\subsection{Ničelna začetna hitrost, degeneracija}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.8\textwidth]{open.png}
    \end{center}
    \caption{Rešitve~(\ref{eq:open-w}) za hitrost (levo) in pot (desno) pri ekstremalni hitrosti $dot{w}(1) = 0$ (odprt robni pogoj). Imamo le en prost parameter $w_0$. Takoj tudi že opazimo, da za zadostno velik $w_0$ pravzaprav \textit{overshoot}-amo semafor, ta problem naslovimo posebej v poglavju~\ref{sec:overshoots}.}
    \label{fig:open}
\end{figure}

Pri uvedbi brezdimenzijskih količin v~\ref{eq:nondim} smo implicitno predpostavili, da sta $T, v_0$ neničelna. Za $T$ je to smiselna predpostavka (in bila bi tudi za $L$), a povsem naraven je primer, ko je $v_0 = 0$. Pri uporabi brezdimenzijskih količin $(\tau, u, \eta)$ bi morali primer $v_0 = 0$ razrešiti posebej -- a bolj elegantno lahko vpeljemo bolj robustne brezdimenzijske količine, ki se tej degeneraciji izognejo
\begin{equation}
    \tau \das \frac{t}{T}, \quad w \das \frac{v}{L/T}, \quad w_0 \das \frac{v_0}{L/T}.
    \label{eq:nondim2}
\end{equation}
V novih količninah $(\tau, w, w_0)$ lahko Lagrangian zapišemo kot
\begin{equation}
    \Ell(w(\tau), \dot{w}(\tau)) \das \dot{w}^2 - \Lambda w,
    \label{eq:L-new}
\end{equation}
pri čemer smo podobno kot pri starih količinah vpeljali nov multiplikator $\Lambda \das \frac{T^3}{L}\lambda$. Sama diferencialna enačba za rešitve ostaja enaka kot~(\ref{eq:ODE1}), le z $\Lambda$ namesto $\widetilde{\lambda}$ in z robnimi pogoji
\begin{equation}
    \text{odprta robna pogoja: } w(0) = w_0, \quad \dot{w}(1) = 0,
    \label{eq:open-boundary}
\end{equation}
s čimer je rešitev oblike
\begin{equation}
    w(\tau) = w_0 + \frac{\Lambda}{4} \left(2\tau - \tau^2 \right),
    \label{eq:open-lambda-ODE}
\end{equation}
izmed njih pa izberemo tisto, ki ustreza vezi $\int_0^1 w \,\diff \tau = 1$. Z vstavljanjem dobljenega $\Lambda$ nazaj v izraz za $w(\tau)$, dobimo odvisnost hitrosti
\begin{equation}
    w(\tau) = w_0 + (1-w_0) \left( 3\tau - \frac{3\tau^2}{2} \right).
    \label{eq:open-w}
\end{equation}
da smo z uvedbo drugih brezdimenzijskih enot, kjer je namesto brezdimenzijske poti $\eta$ ključni parameter brezdimenzijska začetna hitrost $w_0$, pridobili lepše obnašanje v degeneriranem primeru $v_0 = 0$. Na sliki~\ref{fig:open} lahko vidimo obliko rešitve~(\ref{eq:open-w}), ko spreminjamo začetno hitrost $w_0$.

\subsection{Fiksna hitrost pri semaforju}

Alternativa odprtemu robnemu pogoju $\dot{w}(1)=0$ je t. i. zaprto robni pogoj, kjer je končna hitrost pri semaforju točno določena. Spet rešujemo navadno diferencialno enačbo~(\ref{eq:ODE1}), le z robnimi pogoji
\begin{equation}
    \text{zaprta robna pogoja: } w(0) = w_0, \quad \dot{w}(1) = w_1.
    \label{eq:closed-boundary}
\end{equation}
Rešitev enačbe
\begin{equation}
    w(\tau) = w_0 + 2\tau (3-2w_0-w_1) - 3\tau^{2} (2-w_0-w_1).
    \label{eq:closed-w}
\end{equation}
Rešitev~(\ref{eq:closed-w}) je parametrizirana z dvema prostima parametroma $w_0, w_1$, poteke za nabor teh dveh parametrov narišemo na sliki~\ref{fig:closed}.

\subsection{Višje potence pospeška}

Raziskati bi želeli še rešitve z Lagrangianom, ki kaznuje člen ${\dot{w}}^p$ za splošno potenco $p$, ne le za $p=2$. Zapišemo
\begin{equation}
    \Ell_p(w(\tau), \dot{w}(\tau)) \das \dot{w}^{p} - \Lambda w, \quad p \in \mathbb{N}.
    \label{eq:L-2p}
\end{equation}
Lagrangian oblike~\ref{eq:L-2p} nam preko Euler-Lagrange enačbe porodi diferencialno enačbo
\begin{equation}
    p(p-1) \dot{w}^{(p-2)} \ddot{w} = -\Lambda.
\end{equation}
Če enačbo rešujemo kar v tej obliki z \texttt{DSolve} in odprtim robnim pogojem~(\ref{eq:open-boundary}), nam Mathematica izpljune zelo grdo rešitev. Pomagamo ji tako, da enačbo razdelimo na dve enačbi
\begin{align*}
    \dot{Z}(\tau) &= -\Lambda, \quad\quad Z(1) = 0,\\
    Z(\tau) &= p \dot{w}^{(p-1)}, \quad w(0) = w_0,
\end{align*}
tako dobimo pomožno rešitev $Z(t) = \Lambda (1-\tau)$, na podlagi tega pa Mathematica lepo reši drugo enačbo
\begin{equation*}
    \Lambda (1-\tau) = p \dot{w}^{(p-1)}, \quad w(0) = w_0.
\end{equation*}
Glede te enačbe lahko omenimo še en detajl. Ker je čas $\tau \in [0, 1]$, mora biti za $p > 2$ naš $\Lambda > 0$, drugače enačba ni rešljiva v $\mathbb{R}$. Ta pomislek tudi upraviči, da ignoriramo opozorilo, ki nam ga vrne \texttt|DSolve|,sicer:

\medskip
\noindent
\texttt{Solve: Inverse functions are being used by Solve, so some solutions may not be found; use Reduce for complete solution information.}
\medskip

\noindent
Dobljeno rešitev malce poenostavimo na roke in zapišemo
\begin{equation}
    w(\tau) = w_0 + \frac{p-1}{p} \left( \frac{\Lambda}{p} \right)^{\frac{1}{p-1}} \left( 1 - \left[ 1-\tau \right]^{\frac{p}{p-1}} \right).
    \label{eq:2p-lambda-ODE}
\end{equation}
Takoj lahko preverimo, da je za $p=2$ enačba~(\ref{eq:2p-lambda-ODE}) kar enake oblike kot~(\ref{eq:open-lambda-ODE}). Preostane nam le še vez $\int_0^1 w\,\diff \tau = 1$. Ko integiramo~(\ref{eq:2p-lambda-ODE}), upoštevamo že prej upravičeno predpostavko $\Lambda > 0$. Ko dobljeno enačbo rešimo za $\Lambda$ in vsavimo nazaj v~(\ref{eq:2p-lambda-ODE}), dobimo končno obliko rešitve
\begin{equation}
    w(\tau) = w_0 + \frac{(2p-1)(1-w_0)}{p} \left( 1 - \left[ 1-\tau \right]^{\frac{p}{p-1}} \right).
    \label{eq:2p-w}
\end{equation}
Na sliki \ref{fig:L-p} vidimo, kako povečevanje eksponenta $p$ vpliva na optimalne rešitve. Ko se $p$ veča, postajajo rešitve vedno bolj podobne limitni premici
\begin{equation*}
    w(\tau) = w_0 + 2t(1-w_0).
\end{equation*}
Še vedno pa (vsaj v limitnem smislu) velja, da je $\dot{w}(1) = 0$, čeprav je padec na $\dot{w} = 0$ vedno bolj oster za višje $p$ (slika~\ref{fig:L-p} spodaj).
\begin{figure}
    \begin{center}
        \includegraphics[width=0.8\textwidth]{p-exponent.png}
    \end{center}
    \caption{(Zgoraj) Potek hitrosti za odprte robne pogoje in za Lagrangian s splošnim eksponentom $p$. (Spodaj) Potek pospeška. Za večje $p$ je potek vedno bolj linearen, a proti $\tau \to 1$ še vedno zavije tako, da je $\dot{w}(1) = 0$.}
    \label{fig:L-p}
\end{figure}

\subsection{Kvadratni člen v hitrosti}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.9\textwidth]{w2-triple.png}
    \end{center}
    \caption{Poteki hitrosti za odprt robni pogoj z dodanim $\chi^2 w^2$ členom v Lagrangianu~\ref{eq:L-chi}, ki kaznuje višjo hitrost, ne zgolj višjega pospeška. Z višanjem $\chi^2$ vidimo pojav skoraj konstantnih odsekov v $w(\tau)$, saj je tako komulativni $w^2$ minimiziran.}
    \label{fig:w2-triple}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.8\textwidth]{w2-double.png}
    \end{center}
    \caption{Vpliv velikosti parametra $\chi$ za odprt robni pogoj in z Lagrangianom~\ref{eq:L-chi}, ki kaznuje višjo hitrost, ne zgolj višjega pospeška.}
    \label{fig:w2-double}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.9\textwidth]{w2-triple-closed.png}
    \end{center}
    \caption{Poteki hitrosti za zaprt robni pogoj z dodanim $\chi^2 w^2$ členom v Lagrangianu~\ref{eq:L-chi}, ki kaznuje višjo hitrost, ne zgolj višjega pospeška.}
    \label{fig:w2-triple-closed}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.9\textwidth]{w2-double-closed.png}
    \end{center}
    \caption{Vpliv velikosti parametra $\chi$ za zaprt robni pogoj in z Lagrangianom~\ref{eq:L-chi}, ki kaznuje višjo hitrost, ne zgolj višjega pospeška. Tu še bolj vidimo tendenco k poteku hitrosti, ki je čim večji čas konstantna, saj to minimizira komulativni $w^2$.}
    \label{fig:w2-double-closed}
\end{figure}

Če poleg velikosti pospeška smatramo kot neuodobno tudi višjo absolutno hitrost, lahko zapišemo Lagrangian z dodatnim $w^2$ členom
\begin{equation}
    \Ell_\chi(w(\tau), \dot{w}(\tau)) \das \dot{w}^2 + \chi^2 w^2 - \Lambda w.
    \label{eq:L-chi}
\end{equation}
Euler-Lagrange enačbe za tak Lagrangian nam dajo
\begin{equation}
    \ddot{w} - \chi^2 w = -\frac{\Lambda}{2}.
    \label{eq:ODE2}
\end{equation}
Če diferencialno enačbo~(\ref{eq:ODE2}) rešujemo z odprtim robnim pogojem, dobimo rešitev oblike
\begin{equation}
    w(\tau) = e^{-\Lambda\chi\tau} \frac{\Lambda e^{\chi\tau} + \Lambda e^{\chi(2+t)} - [\Lambda - 2\chi^2 w_0](1 + e^{\chi t})e^{\chi t} }{2\chi^2 (1+e^{2\chi})}.
\end{equation}
Ko upoštevamo vez za razdaljo, dobimo za multiplikator vrednost
\begin{equation}
    \Lambda = \frac{2\chi^2 (\chi - w_0 \tanh\chi)}{\chi - \tanh\chi}.
\end{equation}
Tako dobljen $w(\tau)$ narišemo na sliki~\ref{fig:w2-triple}. Na sliki~\ref{fig:w2-double} še bolj eksplicitno pokažemo vpliv večjega $\chi^2$\footnote{Tega ni mešati z $\chi^2$ faktorjem iz statistike, moja izbira simbola je bila morda malce nesrečna.} faktorja na potek $w(\tau)$. Vidimo, da se ob večjem $\chi$ (bolj močno kaznovana hitrost) začne pojavljati v osrednjem delu $\tau \in (0, 1)$ vedno bolj izrazit plato (ang. \textit{plateau}), kjer je $w$ konstantna. Ker zahtevamo neko določeno površino pod krivuljo $w(\tau)$, hitrost $w$ kaznujemo s kvadratnim pribitkom $w^2$, je optimalna oblika kar konstanta. Vse druge oblike so manj optimalne, saj je pribitek na posamezne dele, ki štrlijo ven iz platoja, kvadraten.

Poglejmo si poteke hitrosti, ki ustrezajo Lagrangianu s členom $\chi^2 w^2$ še za zaprt robni pogoj~(\ref{eq:closed-boundary}). Temu ustreza rešitev~(\ref{eq:ODE2}) diferencialne enačbe
\begin{align}
    w(\tau) &= \Lambda e^{-\chi\tau} \frac{e^\chi - e^{2\chi} - e^{\chi t} + e^{2\chi t} + e^{2\chi + \chi \tau} - e^{\chi + 2\chi \tau}}{2\chi^2(e^{2\chi} - 1)}. \nonumber \\
        &+ e^{-\chi\tau} \frac{2w_0 e^{2\chi} - 2w_0 e^{2\chi\tau} -2 w_1 e^{\chi} + 2 w_1 \chi^2 e^{\chi + 2\chi\tau}}{2(e^{2\chi} - 1)}.
\end{align}
Ob upoštevanju vezi~(\ref{eq:vez}) dobimo za multiplikator
\begin{equation}
    \Lambda = \frac{2\chi^2 \left( \chi - w_0 \tanh \frac{\chi}{2} - w_1 \tanh \frac{chi}{2} \right)}{\chi - 2\tanh \frac{\chi}{2}}.
\end{equation}
Za kar nekaj vrednosti parametrov $(\chi^2, w_0, w_1)$ narišemo poteke na slikah~\ref{fig:w2-triple-closed},~\ref{fig:w2-double-closed}. Spet vidimo oblikovanje jasnih platojev za večje $\chi$ (bolj močno kaznovana hitrost). Če bi kaznovali le hitrost (limita $\chi \to \infty$), bi optimalna oblika bila konstanta $w = 1$, s koračnim skokom iz $w_0$ na $1$ pri $\tau = 0$ in spet skokom iz $w = 1$ na $w_1$ pri $\tau = 1$. Ta dva koračna skoka bi predstavljala dve delta funkciji za pospešek, ko je v Lagrangianu prisoten še $\dot{w}^2$ člen, se to ne zgodi, temveč imamo namesto koračnega vseeno nek gladek prehod. Večji kot je $\chi$, bolj oster je prehod in bolj podoben je koračnemu.

\subsection{Preprečevanje overshootanja} \label{sec:overshoots}

Kot smo to že videli na slikah~\ref{fig:open},~\ref{fig:closed}, se dogaja, da avto pri nekem času $\tau < 1$ pelje mimo semaforja, nato pa šele z vzvratno vožnjo pride na brezdimenzijsko razdaljo $1$, da zadosti vezi~(\ref{eq:vez}). To bomo rešili tako, da bomo določili nekakšne meje na dovoljene parametre pri naši optimizaciji.

\subsubsection{Overshoot z odprtim robnim pogojem}

Iz slike~\ref{fig:open} je vidno, da so vso poteki $w(\tau)$ strogo monotoni (ali pa so konstanta). Tedaj je problem \textit{overshoot}-anja semaforja rešen točno takrat, ko se izognemo negativnim hitrostim. Če za rešitev poteka za odprte robne pogoje~(\ref{eq:open-w}) izračunamo diskriminanto, dobimo
\begin{equation*}
    D = 3(3-4w_0+w_0^2),
\end{equation*}
iz česar lahko določimo, da mora biti začetna hitrost\footnote{Tega sicer nismo eksplicitno zapisali, a negativnih $w_0$ ne obravamo.} avtomobila navzgor omejena kot
\begin{equation*}
    w_0 < w_0^{(max.)} = 3, \; \text{da preprečimo \textit{overshoot}}.
\end{equation*}

\subsection{Zaporedni semaforji}

Zanimajo nas še načini vožnje skozi več zaporednih semaforjev. Takoj očitna možnost, ki se ponuja, so zlepljene rešitve za odprt robni pogoj ali zlepljene rešitve za zaprt robni pogoj, pri čemer je $w_1$ (tako se vožnja ustali takoj po prvem odseku). Grafično ta dva poteka vidimo na~\ref{fig:zlepki-closed}~in~\ref{fig:zlepki-closed}.

Alternativno enačbo rešujemo tako, da robne pogoje določimo kar iz zveznosti in zvezne odvedljivosti. To dosežemo tako, da izberemo nek $w_{00}$ in nek $\dot{w}_{00}$, nato pa rešimo enačbo~(\ref{eq:ODE1}), apliciramo vez~(\ref{eq:vez}), nato pa začetne pogoje za naslednji odsek razberemo kar iz te funkcije na naslednjem robu $\tau_n + 1$. Tako rešitev za $w_0 = 2$ si pogledamo za nekaj različnih $\dot{w}_{00}$ na sliki~\ref{fig:periodic}. Vidimo, da rešitev zna kar podivjati, razen za en ozek obseg parametrov.

\begin{figure}
    \begin{center}
        \includegraphics[width=0.8\textwidth]{zlepki-open.png}
        \includegraphics[width=0.8\textwidth]{zlepki-open-s.png}
    \end{center}
    \caption{(Zgoraj) Zlepljene rešitve za odprt robni pogoj in, t. j. funkcije oblike~(\ref{eq:open-w}). (Spodaj) Potek poti za isto rešitev. Vidimo, da hitrost pri semaforju oscilira in se alternirajoče približuje ravnovesni legi $w=1$}
    \label{fig:zlepki-open}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.8\textwidth]{zlepki-closed.png}
        \includegraphics[width=0.8\textwidth]{zlepki-closed-s.png}
    \end{center}
    \caption{(Zgoraj) Zlepljene rešitve za zaprt robni pogoj, $w_1 = 1$ in $w_0 = 5$, t. j. funkcije oblike~(\ref{eq:closed-w}). (Spodaj) Potek poti za isto rešitev. Ta rešitev je relativno nezanimiva, saj se dinamika hitrosti ustali takoj po prvem odseku $[0, 1]$ (ker je na vseh odsekih $w_1 = 1$), nato pa vožnjo nadaljujemo s konstantno hitrostjo.}
    \label{fig:zlepki-closed}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.95\textwidth]{periodic.png}
        \includegraphics[width=0.95\textwidth]{smooth-narrow.png}
    \end{center}
    \caption{(Zgoraj) Gladko zlepljena rešitev za $w_{00} = 2$ in nekaj različnih vrednosti $\dot{w}_{00}$. (Spodaj) Gladko zlepljene rešitve za $w_{00} = 2$ in za ožji nabor parametra $\dot{w}_{00} \in [-3.5, 3]$ (barve gredo od vijolične za $\dot{w}_{00} = -3.5$ do rdeče za $\dot{w}_{00} = -3$).}
    \label{fig:periodic}
\end{figure}

\subsubsection{Overshoot z zaprtim robnim pogojem}

Pojav \textit{overshoot}-anja se prav tako kot pri odprtih pogojih pojavi tudi pri zaprtih robnih pogojih (slika~\ref{fig:closed}). A ker imamo zdaj dva prosta parametra $w_0, w_1$ (namesto le enega), je določitev omejitve za preprečevanje \textit{overshoot}-anja nekoliko težje. Ker poznamo potek hitrosti~(\ref{eq:closed-w}), lahko zapišemo tudi $s(\tau)$, ki je nek polinom 3. stopnje. Ta polinom vedno poteka skozi točko $(\tau, s) = (1, 1)$, zato lahko njegovo obliko poenostavimo kot
\begin{equation*}
    s(\tau) - 1 = (\tau - 1) Q(\tau),
\end{equation*}
pri čemer je $Q(\tau)$ nek kvadratični polinom
\begin{equation*}
    Q(\tau) = 1 - t(1-w_0) - t^2(2 -w_0 - w_1),
\end{equation*}
z ničlami tam, kjer je dosežena razdalja $s(\tau) = 1$. Če je katera od teh dveh ničel $\tau_{1,2}^{(Q)} \in (0, 1)$, je potek problematičen, saj semafor prevozimo pred $\tau = 1$. Na sliki~\ref{fig:discriminant} je narisana diskriminanta $Q(\tau)$ po parametrih $(w_0, w_1)$.
\begin{figure}
    \begin{center}
        \includegraphics[width=0.4\textwidth]{closed-discriminant.png}
    \end{center}
    \caption{Diskriminanta polinoma $Q(\tau)$ za parametra $w_0, w_1$. Z modro črtkano je narisana ničelna diskriminanta, ničle $Q(\tau)$ so možne le pod to krivuljo. Z rdečo šrafirano je označeno območje, kjer dejansko opazimo \textit{overshoot} obnašanje. Tega območja se moramo izogibati, če naj semaforja ne bi prevozili pred časom $\tau = 1$.}
    \label{fig:discriminant}
\end{figure}
Zanima nas območje oz. pogoj za parametre, kjer je prisotno \textit{overshoot} obnašanje. Prvi pogoj dobimo iz diskriminante $Q(\tau)$ -- če naj bo prisoten \textit{overshoot}, bo $Q(\tau)$ imel dve ničli. Pogoj, da ima $Q(\tau)$ dve ničji, je
\begin{equation*}
    w_1 < \frac{1}{4} \big(9 - 6w_0 + w_0^2\big).
\end{equation*}
Potencialno območje je torej pod to krivuljo (modra črtkana parabola na sliki~\ref{fig:discriminant}). Kot opisano na sliki~\ref{fig:proof-by-calc}, pa tudi območje $w_0 < 3$ pod krivuljo ni problematično. Območje \textit{overshoot} obnašanja je torej le območje, označeno z rdečo šrafirano na sliki~\ref{fig:discriminant}. Komplement tega pogoja nam omogoči, da se \textit{overshoot}-anju izognemo, sicer
\begin{equation*}
    w_0 < 3 \quad \text{ali} \quad w_1 \ge \frac{1}{4} \big(9 - 6w_0 + w_0^2\big), \; \text{da preprečimo \textit{overshoot}}.
\end{equation*}
\begin{figure}
    \begin{center}
        \includegraphics[width=0.3\textwidth]{proof-by-calculator.png}
    \end{center}
    \caption{Graf območja, kjer $Q(\tau)$ ima vsaj eno ničlo in je vsaj eden od $\tau^{(Q)}_{1,2} \in (0, 1)$. Kot vidimo, je tak nabor parameter prisoten le za $w_0 \ge 3$. Ker je to določeno z grafičnim izrisom, ostaja možnost, da vseeno obstaja nabor parametrov $w_0, w_1$, ki \textit{overshoot}-ajo semafor, a obstajajo le na neki krivulji ali sporadičnih točkah v $(w_0, w_1)$ in zato niso vidni. A ta možnost se mi zdi le malo verjetna, za dokaz pa nimam časa.}
    \label{fig:proof-by-calc}
\end{figure}

%\nocite{*}
% \emergencystretch=1em
%\printbibliography

\end{document}
