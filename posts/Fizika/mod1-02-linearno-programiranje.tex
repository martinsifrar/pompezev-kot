% !TEX program = xelatex

\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{lit.bib}

% Images and hyperlinks
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathrsfs}

\usepackage{float}

% Custom definitions
% ...
% Differential
\newcommand{\diff}{\mathrm{d}}

\title{
\sc\large Modelska analiza I\\
\bigskip
\bf\Large 2.~naloga: Linearno programiranje
}
\author{Martin Šifrar, 28242021}

\begin{document}
\maketitle

\section{Navodila}

Med tipične primere, ki jih lahko učinkovito rešimo z metodami linearnega programiranja, sodi sestavljanje diet za hujšanje, zdravljenje ali športne aktivnosti. Za dani nabor živil določamo njihove količine, pri čemer moramo zadostiti različnim omejitvam. Med drugim moramo zagotoviti priporočene dnevne odmerke mineralov, vitaminov in hranilnih snovi, omejiti pri vnos maščob, ogljikovih hidratov ter telesu škodljivih snovi, hkrati pa zagotoviti, da energijska vrednost ustreza zahtevam posameznika. Vnos vsake izmed hranilnih snovi je linearna funkcija količin živil in je natanko določena z njihovo sestavo. Od vrste diete pa je odvisno, katere parametre omejimo in katere minimiziramo. Datoteka \texttt{tabela-zivil.dat} vsebuje podatke o energijski vrednosti ter vsebnosti maščob, ogljikovih hidratov, proteinov, kalcija in železa v nekaj živilih, skupaj z okvirnimi podatki o njihovi ceni.

\section{Rešitev}

Sestava diete je povsem specificirana z masami vsakega izmed tipov hrane, ki jih uživamo. Imenujmo to za vektor
\begin{equation*}
    \mathbf{x} = \begin{bmatrix}
        x_1 \\
        x_2 \\
        x_3 \\
        \vdots
    \end{bmatrix}
    = \begin{bmatrix}
        m_\mathrm{jabolko} \\
        m_\mathrm{pomfri} \\
        m_\mathrm{govedina} \\
        \vdots
    \end{bmatrix}.
\end{equation*}
Za našo dieto imamo nekaj linearnih zahtev, npr. želimo, da dieta zagotovi zadosten vnos železa. To izrazimo kot linearno vez
\begin{align}
    m_{\mathrm{Fe}} &= w^\mathrm{Fe}_\mathrm{jabolko} m_\mathrm{jabolko} + w^\mathrm{Fe}_\mathrm{pomfri} m_\mathrm{pomfri} + w^\mathrm{Fe}_\mathrm{govedina} m_\mathrm{govedina} + \dots \nonumber \\
                    &\le 18\,\mathrm{mg},
    \label{eq:vez}
\end{align}
pri čemer so $w^\mathrm{Fe}$ specifične (na $100\,\mathrm{g}$) vsebnosti železa v tem konkretnem živilu (npr. $1.94\,\mathrm{mg}/100\,\mathrm{g}$ za govedino). Take specifične vsebnosti $w^i_j$ za hranilne snovi $i$ in živila, ki jih vsebujejo $j$, imamo podane v \texttt{tabela-zivil.dat}, ilustrirano pa z nekaj podatki v tabeli~\ref{tab:data}.
\begin{table}
    \centering
    \begin{tabular}{c|r|r|r}
        živilo & energija $[\mathrm{kcal}]$ & maščobe $[\mathrm{g}]$ & $\mathrm{Fe}\,[\mathrm{mg}]$ \\
        \hline
        Jabolko & 52 & 0.17 & 0.12 \\
        Pomfri & 93 & 12 & 0.35 \\
        Govedina & 254 & 20 & 1.94
    \end{tabular}
    \caption{Majhen vzorec podatkov iz tabele~\texttt{tabela-zivil.dat}.}
    \label{tab:data}
\end{table}
Vez~(\ref{eq:vez}) lahko izrazimo tudi kot skalarni produkt
\begin{equation*}
    \mathbf{w}_\mathrm{Fe}^T \mathbf{x} \le 18\,\mathrm{mg},
\end{equation*}
pri čemer je $\mathbf{w}_\mathrm{Fe}$ vektor vsebnosti železa za vsa popisana živila (torej stolpec tabele~\ref{tab:data} pod $\mathrm{Fe}$). Pravzaprav specificiramo poljubno takih vezi, skupno pa to zapišemo v matrični obliki. Npr. če želimo omejiti železo in natrij kot
\begin{equation*}
    m_\mathrm{Fe} \ge 18\,\mathrm{mg}, \qquad 500\,\mathrm{mg} \le m_\mathrm{Na} \le 2400\,\mathrm{mg},
\end{equation*}
lahko vse to izrazimo ekvivalentno v matrični obliki\footnote{Izbrana je taka, da se ujema z SciPy API-jem.} kot
\begin{equation*}
    \begin{bmatrix}
        -\mathbf{w}_\mathrm{Fe}^T \\
        -\mathbf{w}_\mathrm{}^T \\
        \mathbf{w}_\mathrm{}^T \\
    \end{bmatrix}
    \le
    \begin{bmatrix}
        -18\,\mathrm{mg} \\
        -500\,\mathrm{mg} \\
        2400\,\mathrm{mg} \\
    \end{bmatrix},
\end{equation*}
pri čemer razumemo, da omejitev velja za vsak element vektorja posebej.

\subsection{Dieta za hujšanje}

\begin{figure}
    \begin{center}
        \includegraphics[scale=0.88]{min_calories.2000.plot.pdf}
        \includegraphics[scale=0.88]{min_calories.500.plot.pdf}
        \includegraphics[scale=0.88]{min_calories.200.plot.pdf}
    \end{center}
    \caption{Dieta z omejitvami~(\ref{eq:manj-omejitev}) in kriterijem najmanjše količine zaužitih kalorij. (Zgoraj) Dieta z neomejeno količino posameznih živil. (Sredinsko in Spodaj) Dieta, kjer dovolimo največ $500\,\mathrm{g}$ oz. največ $200\,\mathrm{g}$ posameznega živila.}
    \label{fig:min-calories}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[scale=0.88]{min_calories_sodium_limit.2000.plot.pdf}
        \includegraphics[scale=0.88]{min_calories_sodium_limit.500.plot.pdf}
        \includegraphics[scale=0.88]{min_calories_sodium_limit.200.plot.pdf}
    \end{center}
    \caption{Dieta z omejitvami~(\ref{eq:manj-omejitev}) in~(\ref{eq:dodatne-omejitve}) in kriterijem najmanjše količine zaužitih kalorij. (Zgoraj) Dieta z neomejeno količino posameznih živil. (Sredinsko in Spodaj) Dieta, kjer dovolimo največ $500\,\mathrm{g}$ oz. največ $200\,\mathrm{g}$ posameznega živila.}
    \label{fig:min-calories-sodium}
\end{figure}

Prav tako kot vez~(\ref{eq:vez}), mora biti tudi funkcija, ki jo minimiziramo, linearna funkcija. Če recimo minimiziramo zaužite kalorije, je minimizacijska funkcija (ang. \textit{cost} funkcija) oblike
\begin{align}
    c_{\mathrm{cal}} &= w^\mathrm{cal}_\mathrm{jabolko} m_\mathrm{jabolko} + w^\mathrm{cal}_\mathrm{pomfri} m_\mathrm{pomfri} + w^\mathrm{cal}_\mathrm{govedina} m_\mathrm{govedina} + \dots \nonumber \nonumber \\
                     &= \mathbf{w}_\mathrm{cal}^T \mathbf{x},
                     \label{eq:lin-model}
\end{align}
pri čemer je $\mathbf{w}_\mathrm{cal}$ kar prvi stolpec tabele~\ref{tab:data}. Z minimizacijo zaužitih kalorij in omejitvami
\begin{align}
    m_\mathrm{maščobe} &\ge 70\,\mathrm{g} \nonumber \\
    m_\mathrm{OH} &\ge 310\,\mathrm{g} \nonumber \\
    m_\mathrm{proteini} &\ge 50\,\mathrm{g} \nonumber \\
    m_\mathrm{Ca} &\ge 1000\,\mathrm{mg} \nonumber \\
    m_\mathrm{Fe} &\ge 18\,\mathrm{mg},
    \label{eq:manj-omejitev}
\end{align}
dobimo dieto na sliki~\ref{fig:min-calories} zgoraj. Takoj zaznamo pomenljivost modela v tem, da ne omejujemo količine zaužitega natrija. Če bi lahko brez težav zaužili večje količine soli, bi to namreč predstavljalo ugoden nizko-kaloričen vir kalcija (slika~\ref{fig:min-calories} zgoraj). Zato osnovnim omejitvam~(\ref{eq:manj-omejitev}) dodamo še omejitve
\begin{align}
    m_\text{vitamin C} &\ge 60\,\mathrm{mg} \nonumber \\
    m_\text{K} &\ge 3500\,\mathrm{mg} \nonumber \\
    m_\text{Na} &\in [500\,\mathrm{mg}, 2400\,\mathrm{mg}]
    \label{eq:dodatne-omejitve}
\end{align}
tako pa dobimo nekoliko bolj realistično dieto na sliki~\ref{fig:min-calories-sodium} zgoraj. Vidimo, da je radenska nizko-kaloričen (oz. nič-kaloričen) vir kalcija in natrija, kakav pa v dieti nastopa kot nekakšno super-živilo, saj preko $500\,\mathrm{g}$ kakava pridobimo večino drugih potrebnih hranil, razen vitamina C (tega pridobimo preko pomaranče).

\subsection{Omejevanje količine posameznega živila}

Depresivna je misel na dieto, kjer zaužijemo nesorazmerno veliko količino enega živila. Zato uvedemo maksimalno količino za vsako posamezno živilo. Za živila $j = 1, 2, 3, \dots$ to izrazimo matrično kot vezi
\begin{equation*}
    \begin{bmatrix}
        -\mathbf{e}_\mathrm{1}^T \\
        -\mathbf{e}_\mathrm{2}^T \\
        -\mathbf{e}_\mathrm{3}^T \\
        \vdots
    \end{bmatrix}
    \le
    \begin{bmatrix}
        m_\text{max. na živilo} \\
        m_\text{max. na živilo} \\
        m_\text{max. na živilo} \\
        \vdots
    \end{bmatrix},
\end{equation*}
torej kar z $I_N$ identitetno matriko za $N$ različnih živil. Vse diete, ki jih bomo določili, izračunamo še s to dodatno omejitvijo. Tako pridobljene diete za omejitvi $500\,\mathrm{g}$ in $200\,\mathrm{g}$ na posamezno živilo vidimo na kot sredinsko in spodnjo figuro na slikah~\ref{fig:min-calories},~\ref{fig:min-calories-sodium},~\ref{fig:min-fat},~\ref{fig:min-price} in~\ref{fig:min-price}.

\subsection{Minimizacija zaužitih maščob}

Kot alternativo dieti za hujšanje, kjer zahtevamo čim manjše število zaužitih kalorij, lahko zahtevamo čim manjšo količino zaužitih maščob. Pri tem dodatno omejimo število kalorij, tako da ustreza ravnovesnemu dnevnemu vnosu
\begin{equation*}
    c_\mathrm{cal} \ge 2000.
\end{equation*}
Dieta za čim manjšo količino maščob je prikazana na sliki~\ref{fig:min-fat}.

\begin{figure}
    \begin{center}
        \includegraphics[scale=0.88]{min_fats.2000.plot.pdf}
        \includegraphics[scale=0.88]{min_fats.500.plot.pdf}
        \includegraphics[scale=0.88]{min_fats.200.plot.pdf}
    \end{center}
    \caption{Dieta z omejitvami~(\ref{eq:manj-omejitev}) in~(\ref{eq:dodatne-omejitve}), le brez omejitve maščob in z zahtevo $c_\mathrm{cal} \ge 2000$. Kriterij za minimizacijo je najmanjše količine zaužitih maščob. (Zgoraj) Dieta z neomejeno količino posameznih živil. (Sredinsko in Spodaj) Dieta, kjer dovolimo največ $500\,\mathrm{g}$ oz. največ $200\,\mathrm{g}$ posameznega živila.}
    \label{fig:min-fat}
\end{figure}

\subsection{Minimizacija cene, mornarska dieta}

Še en zanimiv kriterij za minimizacijo je cena. Poglejmo si torej, kakšno dieto dobimo, če minimiziramo ceno, hkrati pa še vedno zahtevamo kalorični vnos $c_\mathrm{cal} \ge 2000$. Dieto, ki jo dobimo (slika~\ref{fig:min-price}), bi lahko poimenovali \blockquote{mornarska dieta}. Primarno jo namreč sestavlja poceni vir ogljikovih hidratov in beljakovin (oves), ki je primeren tudi za dolgoročno shrambo. Dopolnimo ga z mlekom, ki bi ga lahko dobili npr. od koze na ladji. Pomemben in zanimiv pa je tudi dodatek zelja, ki je primarni vir vitamina C, kot kislo zelje v sodih zopet primeren za dolgoročno shrambo.

\begin{figure}
    \begin{center}
        \includegraphics[scale=0.88]{min_price.2000.plot.pdf}
        \includegraphics[scale=0.88]{min_price.500.plot.pdf}
        \includegraphics[scale=0.88]{min_price.200.plot.pdf}
    \end{center}
    \caption{Dieta z omejitvami~(\ref{eq:manj-omejitev}) in~(\ref{eq:dodatne-omejitve}) in z zahtevo $c_\mathrm{cal} \ge 2000$. Kriterij za minimizacijo je najnižja cena živil. (Zgoraj) Dieta z neomejeno količino posameznih živil. (Sredinsko in Spodaj) Dieta, kjer dovolimo največ $500\,\mathrm{g}$ oz. največ $200\,\mathrm{g}$ posameznega živila.}
    \label{fig:min-price}
\end{figure}

\subsection{Realistična dieta}

Poskusimo z linearnim modelom predpisati še nekoliko bolj realistično dieto. Naš cilj lahko ostane najmanjše število zaužitih kalorij, torej je cilj hujšanje. Ker je pri hujšanju velik problem neželena izguba mišične mase, bomo nekoliko povečali zahtevano količino beljakovin
\begin{align}
    m_\mathrm{maščobe} &\ge 70\,\mathrm{g} \nonumber \\
    m_\mathrm{OH} &\ge 300\,\mathrm{g} \nonumber \\
    m_\mathrm{proteini} &\ge 120\,\mathrm{g} \nonumber \\
    m_\mathrm{Ca} &\ge 1000\,\mathrm{mg} \nonumber \\
    m_\mathrm{Fe} &\ge 18\,\mathrm{mg} \nonumber \\
    m_\text{vitamin C} &\ge 60\,\mathrm{mg} \nonumber \\
    m_\text{K} &\ge 3500\,\mathrm{mg} \nonumber \\
    m_\text{Na} &\in [500\,\mathrm{mg}, 2400\,\mathrm{mg}].
    \label{eq:real-omejitve}
\end{align}
Iz prejšnjih poskusov očitno, da kakav dominira kot nizkokalorično živilo z zadostno količino beljakovin in maščob. A tako živilo nastopa kot suh prašek, precej neprimeren za uživanje kar v tej obliki. Zato tudi to izločimo iz našega nabora živil. Tako dobljeno \blockquote{realistično} dieto vidimo na sliki~\ref{eq:real-omejitve}.

\begin{figure}
    \begin{center}
        \includegraphics[scale=0.88]{real_min_calories.2000.plot.pdf}
        \includegraphics[scale=0.88]{real_min_calories.500.plot.pdf}
    \end{center}
    \caption{Dieta z nekoliko bolj realističnimi omejitvami~\ref{eq:real-omejitve} kriterijem najmanjše količine zaužitih kalorij. (Zgoraj) Dieta z neomejeno količino posameznih živil. (Sredinsko in Spodaj) Dieta, kjer dovolimo največ $500\,\mathrm{g}$ oz. največ $200\,\mathrm{g}$ posameznega živila.}
    \label{fig:real-min-cals}
\end{figure}

\pagebreak

\section{Problem vožnje skozi semaforje}

Zdaj bomo s podobno tehniko rešili še problem iz prejšnje naloge\footnote{Za navodilo in splošno formulacijo problema glej nalogo 101.}. Zelo blizu in podobno linearnemu modelu~(\ref{eq:lin-model}) je njegova preprosta razširitev, kjer je kriterij za minimizacijo oblike
\begin{equation*}
    c = \mathbf{q}^T \mathbf{x} + \mathbf{x}^T H \mathbf{x},
\end{equation*}
pri čemer je matrika $H$ neka pozitivno semi-definitna matrika, t. j. kvadratna forma $\mathbf{x}^T H \mathbf{x}$ predstavlja nek minimum, ne maksimuma ali sedla. Za optimizacijo kumulativnega kvadrata pospeška prvo določimo pospeške preko končnih diferenc (\textit{forward} diferenca 1. reda). V matričnem zapisu
\begin{equation}
    \dot{\mathbf{v}} =
    \begin{bmatrix}
        \dot{w}_1 \\
        \dot{w}_2 \\
        \dot{w}_3 \\
        \vdots
    \end{bmatrix}
    \approx
    N \begin{bmatrix}
        w_2 - w_1  \\
        w_3 - w_2  \\
        w_4 - w_3  \\
        \vdots
    \end{bmatrix}
    =
    N \begin{bmatrix}
        -1 & 1 \\
        0 & -1 & 1 \\
        & 0 & \ddots & \ddots \\
        & & \ddots & \ddots & 1 \\
         & & & 0 & -1 & 1
    \end{bmatrix}
    \begin{bmatrix}
        w_1 \\
        w_2 \\
        w_3 \\
        \vdots
    \end{bmatrix}
    = D_1 \mathbf{w}
    \label{eq:matrix}
\end{equation}

\begin{figure}
    \begin{center}
    \includegraphics{base.plot.pdf}
    \end{center}
    \caption{Poteki hitrosti (Spodaj) in prepotovane poti (Zgoraj) pri optimizaciji kriterija~(\ref{eq:c-qp}) z upoštevanjem vezi~(\ref{eq:vez-0}). Prikazani so poteki za različne začetne hitrosti $w_0$.}
    \label{fig:base}
\end{figure}

Če želimo čim manjši kumulativni kvadrat pospeška, je optimizacijski kriterij
\begin{equation}
    c = \dot{\mathbf{w}}^T \dot{\mathbf{w}} \approx (D_1 \mathbf{w})^T D_1 \mathbf{w} = \mathbf{v}^T H_1 \mathbf{w},
    \label{eq:c-qp}
\end{equation}
pri čemer je Hessova matrika iz same definicije pozitivno semi-pozitivna
\begin{equation*}
    H_1 = D_1^T D_1
    =
    N^2 \begin{bmatrix}
        1 & -1 \\
        -1 & 2 & -1 \\
        & -1 & 2 & -1 \\
        & & -1 & \ddots & \ddots \\
        & & & \ddots & \ddots & -1 \\
        & & & & -1 & 1 \\
    \end{bmatrix},
\end{equation*}
in je precej podobna matriki končnih diferenc za $-\ddot{w}$, razen v zgornjem levem in spodnjem desnem kotu. Pri optimizaciji zahtevamo, da semafor prevozimo ob času $\tau = 1$, kar ustreza linearni vezi
\begin{equation*}
    1 = \int_0^1 w \,\diff\tau \approx \frac{1}{N} \left( w_1 + w_2 + \dots + w_N \right).
    \label{eq:vez-0}
\end{equation*}

\subsection{Igranje z modelom}

Model oblike~(\ref{eq:c-qp}) optimiziramo z modelom iz Python paketa \texttt{qpsolvers}, sicer z \texttt{solver="proxqp"}. Da se z modelom lahko nekoliko igram, sem sestavil preprosto aplikacijo (slika~\ref{fig:screen}), kjer lahko prosto nastavljam parametre in vklapljam in izklapljam vezi.

\begin{figure}
    \begin{center}
        \includegraphics[width=0.6\textwidth]{screen.png}
    \end{center}
    \caption{Preprosta aplikacija za igranje s parametri modela optimalne vožnje. Gumba s kljukicami določata upoštevanje vezi~(\ref{eq:vez-A}) in~(\ref{eq:vez-B}).}
    \label{fig:screen}
\end{figure}

\subsection{Preprečevanje overshootanje, dva načina}

\begin{figure}
    \begin{center}
    \includegraphics{upper-limit-s.plot.pdf}
    \end{center}
    \caption{Poteki hitrosti (Spodaj) in prepotovane poti (Zgoraj) pri optimizaciji kriterija~(\ref{eq:c-qp}) z upoštevanjem vezi za prepoved overshoota pred $\tau < 1$ (vez ~(\ref{eq:vez-A}). Pri testiranju se izkaže, da ima vez~(\ref{eq:vez-B}) pravzaprav povsem ekvivalenten efekt, zato slike nisem ponavljal. Razliko med tema dvema načinoma se pokaže šele na slikah~\ref{fig:fixed-w1}. Prikazani so poteki za različne začetne hitrosti $w_0$.}
    \label{fig:upper-limit-s}
\end{figure}

Če želimo zagotoviti tudi, da pred časom $\tau < 1$ ne prevozimo semaforja, lahko zahtevamo, da je pot manjša od $1$ tudi za integracijske čase $\tau_1, \tau_2, \dots, \tau_{N-1}$. V matrični obliki so to (po elementih) vezi oblike
\begin{equation}
    \frac{1}{N} \begin{bmatrix}
        1 & \dots & 1 & 1 & 1 & 0 \\
        1 & \dots & 1 & 1 & 0 & 0 \\
        1 & \dots & 1 & 0 & 0 & 0 \\
        \vdots \\
        1 & 1 & 0 & \dots & 0 & 0 \\
        1 & 0 & 0 & \dots & 0 & 0 \\
    \end{bmatrix} \mathbf{v} \le 1.
    \label{eq:vez-A}
\end{equation}
Sicer bolj stroga vez kot to, da ne smemo prevoziti semaforja, je totalna prepoved vzvratne vožnje
\begin{equation}
    \mathbf{w} \ge 0.
    \label{eq:vez-B}
\end{equation}
Na prvi pogled se zdi, da je vez~(\ref{eq:vez-B}) strožja kot~(\ref{eq:vez-A}). A igranje z modelom na sliki~\ref{fig:upper-limit-s} kaže, da delujeta povsem ekvivalentno. Šele ko dodamo vez za fiksno končno hitrost
\begin{equation*}
    w_N = w_1,
\end{equation*}
opazimo (slika~\ref{fig:fixed-w1}) razliko med poteki glede na to, ali upoštevamo strožjo vez nenegativnih hitrosti~(\ref{eq:vez-B}) ali manj restriktivno vez~(\ref{eq:vez-A}).
\begin{figure}
    \begin{center}
        \begin{minipage}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{fixed-w1-upper-s.pdf}
        \end{minipage}%
        \begin{minipage}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{fixed-w1-nonnegative-w.pdf}
        \end{minipage}
    \end{center}
    \caption{Poteki hitrosti (Spodaj) in prepotovane poti (Zgoraj) pri optimizaciji kriterija~(\ref{eq:c-qp}). Prikazani so poteki za različne končne hitrosti $w_1$. (Levo) Z upoštevanjem vezi $s(\tau) < 1$ za preprečevanje overshoota v $\tau \in [0, 1)$ (vez ~(\ref{eq:vez-A}). (Desno) Z upoštevanjem vezi $w \ge 1$, ki preprečuje negativne hitrosti (vez ~(\ref{eq:vez-B}).}
    \label{fig:fixed-w1}
\end{figure}

\subsection{Omejitev hitrosti}

V modelu lahko zelo preprosto upoštevamo tudi hitrostne omejitve. Sicer kot
\begin{equation}
    I \mathbf{v} \le v_\text{max}.
    \label{eq:vez-MAX}
\end{equation}
Efekt te omejitve vidimo, ko je začetna hitrost nizka (glede na $1$), omejitev pa je le nekoliko večja od $1$, da ravno še omogoča potovanje za pot $1$ v danem času (slika~\ref{fig:low-speed-limit}). Tedaj je potovanje relativno neudobno, saj je potreben velik pospešek (na omejitev), da lahko semafor še dosežemo.

\begin{figure}
    \begin{center}
    \includegraphics{low-speed-limit.pdf}
    \end{center}
    \caption{Poteki hitrosti (Spodaj) in prepotovane poti (Zgoraj) pri optimizaciji kriterija~(\ref{eq:c-qp}) z upoštevanjem vezi~(\ref{eq:vez-0}) in maksimalne hitrosti $w_\text{max} = 1.1$ (vez~(\ref{eq:vez-MAX})). Prikazani so poteki za različne začetne hitrosti $w_0$.}
    \label{fig:low-speed-limit}
\end{figure}

%\nocite{*}
% \emergencystretch=1em
%\printbibliography

\end{document}
