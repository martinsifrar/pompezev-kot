% !TEX program = xelatex

\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{lit.bib}

% Images and hyperlinks
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathrsfs}
\usepackage{float}

\usepackage{tikz}

% Custom definitions
% ...
% "Defined as" symbol
\usepackage{mathtools}
\newcommand{\das}{\vcentcolon=}
\newcommand{\asd}{=\vcentcolon}
\newcommand{\Ell}{\mathcal{L}}
\newcommand{\Zed}{\mathcal{Z}}

\title{
\sc\large Modelska analiza I\\
\bigskip
\bf\Large 4.~naloga: Populacijski modeli
}
\author{Martin Šifrar, 28242021}

\begin{document}
\maketitle

\section{Navodila}

\begin{enumerate}
    \item Razišči preprost model epidemije. V modelu populacijo razdelimo v tri razrede: (D) zdravi in dovzetni, (B) bolni in kliconosni, (I) imuni: nedovzetni in nekliconosni. Bolezen se širi s stiki med zdravimi in bolnimi. Bolnik preide s konstantno verjetnostjo med imune (ozdravi ali umre).
    \item Preuči standardni standardni deterministični model zajci-lisice (model Lotka-Volterra).
    \item Analiziraj fazni portret za populacijski model laserja s konstantnim črpanjem.
\end{enumerate}

\section{Model epidemije}

\begin{figure}
    \begin{center}
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{base_03_03.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{base_03_01.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{base_08_01.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{Poteki epidemije za $B_0 = 1/100$ in za tri različne parametre $\alpha, \beta$. Vidimo, da večji $\alpha$ (t. j. večji $R_0$) povzroči hitrejše širjenje in višji vrh. Manjši $\beta$ (večji čas okrevanja $\tau_\mathrm{preboli}$) prav tako povzroči višji vrh, a pandemija potem traja dlje časa.}
    \label{fig:base}
\end{figure}

Preprost model epidemije modeliramo kot sistem navadnih diferencialnih enačb
\begin{align}
    \dot{D} &= -\alpha DB, \nonumber \\
    \dot{B} &= \alpha DB - \beta B, \label{eq:SIR} \\
    \dot{I} &= \beta B. \nonumber
\end{align}
Faktor $\alpha$ je pri tem t. i. reproduktivni faktor bolezni, ki narekuje hitrost njenega širjenja. Takoj v prvi enačbi v sistemu vidimo, da se bo število dovzetnih le zmanjševalo
\begin{equation*}
    D(t) \le D_0,
\end{equation*}
posledično pa to, ali se bo epidemija sploh širila, narekuje pogoj
\begin{equation}
    \dot{B} \le I(\alpha D_0 - \beta).
    \label{eq:ineq}
\end{equation}
Če želimo, da se bolezen vsaj na neki točki širi (da je $\dot{B} > 0$), mora biti desna stran neenakosti~(\ref{eq:ineq}) pozitivna, torej mora veljati
\begin{equation}
    R \das \frac{\alpha D_0}{\beta} > 1,
    \label{eq:R}
\end{equation}
pri čemer smo uvedli t. i. reproduktivni faktor $R$. Če je na začetku dovzetna vsa populacija, je to t. i. osnovni reproduktivni faktor.
\begin{equation*}
    R_0 \das R\vert_{D_0 = 1} = \frac{\alpha}{\beta}
\end{equation*}
Ta parameter je široko uporabljen v imunološki literaturi in tudi poročan tekom bolezni kot je bil npr. Covid-19 (glej tabelo~\ref{tab:R0}). Za neke smiselne začetne parametre bomo predpostavili, da tipično ljudje bolezen prebolijo (ali pa umrejo) v času 10-ih dni
\begin{equation*}
    \beta = \frac{1}{\tau_\text{preboli}} \approx \frac{1}{10\,\text{dni}} = 0.1.
\end{equation*}
Če torej vzamemo nek smiseln $R_0 \approx 3$ za SARS-CoV, lahko izberemo za $\alpha \approx 0.3$. Narišemo primere poteke epidemije za različne $\alpha, \beta$ (glej sliko~\ref{fig:base}). Kako točno je vrh $B_\text{max}$ in čas njegovega nastopa odvisen od parametrov epidemije, lahko vidimo na sliki~\ref{fig:by_R0_and_tau}, kjer namesto parametrov $\alpha, \beta$ uporabimo ekvivalentna $R_0, \tau_\text{preboli}$. Vidimo, da večji $\alpha$ epidemijo ojača (večji $B_\text{max}$) in pospeši (manjši $t(B_\text{max})$), medtem pa manjši $\beta$ epidemijo zgolj ojača (večji $B_\text{max}$), časa njenega vrha pa ne spremeni.

\begin{figure}[ht]
    \begin{center}
    \includegraphics{B_max_by_R0.plot.pdf}
    \includegraphics{B_max_by_tau.plot.pdf}
    \end{center}
    \caption{Odvisnost vrha epidemije $B_\mathrm{max}$ in časa, ob katerem se vrh pojavi $t(B_\mathrm{max})$ od reproduktivnega faktorja $R_0$ in časa trajanja bolezni $\tau_\text{preboli}$. (Zgoraj) Preseki za konstantne $\tau_\text{preboli}$. (Spodaj) Preseki za konstantne $R_0$.}
    \label{fig:by_R0_and_tau}
\end{figure}

\begin{table}
    \centering
    \begin{tabular}{c|r}
         &  $R_0$ \\
        \hline
        Wuhan & 2.4-2.6 \\
        EU & 3 \\
        $\alpha$ & 4-5 \\
        $\delta$ & 5-8 \\
        ošpice & 12-18
    \end{tabular}
    \caption{Osnovni reproduktivni faktorji za variante SARS-CoV (Covid-19) virusa in za ošpice. Vzeto iz~\cite{bbc2021}.}
    \label{tab:R0}
\end{table}

\subsection{Odvisnost vrha epidemije}

Zanima nas tudi, koliko je maksimalno število bolnih $B_\mathrm{max}$ in kdaj tak vrh epidemije nastopi, t. j. čas $t(B_\mathrm{max})$. To odvisnost narišemo na sliki~\ref{fig:by_R0_and_tau} po parametrih $(R_0, \tau_\text{preboli})$ (namesto $(\alpha, \beta)$).

\begin{figure}
    \begin{center}
        \includegraphics{segments.plot.pdf}
    \end{center}
    \caption{Potek bolezni, če ob nekem času (v 60. dnevu za prikazan primer) virus mutira in se mu spremeni reproduktivni faktor $R_0$. Vidimo, da lahko iz upadanja preidemo v ponovno rast števila bolnih, dobimo drugotni vrh epidemije.}
    \label{fig:segments}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics{immunity.plot.pdf}
    \end{center}
    \caption{Poteki bolezni za različne začetne deleže imunih $I_0$. Z večjim številom imunih osebkov se vrh epidemije $B_\mathrm{max}$ manjša, za $I_0 > I_0^{(\text{krit})}$ pa se epidemija sploh ne začne, saj je reproduktivni faktor po~(\ref{eq:R}) že takoj manjši od 1.}
    \label{fig:immunity}
\end{figure}

\subsection{Mutacija virusa in cepljenje}

Če virus med potekom epidemije mutira, se mu lahko spremeni npr. reproduktivni faktor (glej tabelo~\ref{tab:R0}). To lahko povzroči sekundarni vrh epidemije, ki je višji od začetno pričakovanega (slika~\ref{fig:segments}).

V model bomo poskusili vključiti tudi pojav cepljenja. V nekem trenutku $\tau_\text{cep.}$ bomo precepili $\Delta I_\text{cep.}$ delež populacije (slika~\ref{fig:vaccine} levo). Kako čas cepljenja in delež cepljenja vpliva na vrh epidemije vidimo na sliki~\ref{fig:vaccine} desno.

\subsection{Inkubacijska doba in popuščanje imunosti}

Po vzoru~\cite{ebraheem2021delayed} dodamo našemu modelu~(\ref{eq:SIR}) še inkubacijsko dobo $\tau_\text{DB}$ in pa člen $\gamma I_{(t - \tau_\text{ID})}$, ki opisuje popuščanje imunosti z specifično verjetnostjo $\gamma$ po času $\tau_\text{ID}$. Po novem se sistem diferencialnih enačb glasi
\begin{align}
    \dot{D} &= -\alpha D_{(t - \tau_\text{DB})} B_{(t - \tau_\text{DB})} + \gamma I_{(t - \tau_\text{ID})}, \nonumber \\
    \dot{B} &= \alpha D_{(t - \tau_\text{DB})} B_{(t - \tau_\text{DB})} - B_{(t - \tau_\text{BI})}, \label{eq:dde} \\
    \dot{I} &= B_{(t - \tau_\text{BI})} - \gamma I_{(t - \tau_\text{ID})}. \nonumber
\end{align}
Rešitve takšnega sistema nam zdaj ponudijo novo oscilatorno dinamiko (slike~\ref{fig:oscillations_A},~\ref{fig:oscillations_B} in~\ref{fig:oscillations_B2}). Ob pravi izbiri parametrov 

\begin{figure}
    \begin{center}
        \begin{minipage}{0.5\textwidth}
            \centering
            \vspace{10pt}
            \includegraphics[width=\textwidth]{vaccination.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{vaccination_params.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{Pulzni model cepljenja, kjer ob času $t_\text{cep.}$ naenkrat precepimo $\Delta I_\text{cep.}$ delež populacije. (Levo) Potek ob pulznim cepljenjem $30\%$ populacije tik pred vrhom epidemije (na 18. dan). (Desno) Vpliv cepljenja na vrh epidemije $B_\text{max}$ glede na čas in delež pulznega cepljenja. Vidimo, da je efekt znatno večji, če cepimo pred vrhom epidemije (za $(\alpha, \beta) = (0.3, 0.1)$ je ta ob $t \approx 26.6\,\mathrm{dni}$).}
    \label{fig:vaccine}
\end{figure}

\section{Zajci in lisice}

Zdaj si bomo pogledali še populacijski model Lotka-Volterra, ki opisuje dinamiko plena (zajci $Z$) in plenilcev (lisice $L$)
\begin{align}
    \dot{Z} &= \alpha Z - \beta ZL, \nonumber \\
    \dot{L} &= -\gamma L + \delta ZL.
    \label{eq:LV}
\end{align}
Uvedli bomo brezdimenzijske količine
\begin{equation*}
    \Zed = \frac{Z}{Z_0}, \quad \Ell = \frac{L}{L_0}, \quad \tau = \frac{t}{t_0},
\end{equation*}
pri čemer sta $Z_0, L_0$ t. i. ravnovesni populaciji plena
\begin{equation}
    Z_0 = \frac{\delta}{\gamma}, \qquad L_0 = \frac{\alpha}{\beta}.
    \label{eq:units}
\end{equation}
Če ju vstavimo v~(\ref{eq:LV}), ne bo nobene dinamike, sistem bo preprosto ostal v tem stanju s konstantnima populacijama zajcev in lisic. V količinah $(\Zed, \Ell, \tau)$ dobimo nov zapis Lotka-Volterra sistema
\begin{align*}
    \dot{\Zed} &= \alpha t_0 \left( \Zed - \Zed \Ell \right), \\
    \dot{\Ell} &= \gamma t_0 \left( \Zed\Ell - \Ell \right).
\end{align*}
Tedaj lahko vpeljemo brezdimenzijski parameter $r$ in karakteristični čas
\begin{equation}
    r \das \sqrt{\frac{\alpha}{\gamma}}, \qquad t_0 = \frac{1}{\sqrt{\alpha \gamma}}.
    \label{eq:r}
\end{equation}

\begin{figure}
    \begin{center}
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{oscillations_A_099.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{oscillations_A_095.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{oscillations_A_080.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{Oscilacije iz~(\ref{eq:dde}) v vrhu epidemije in pojemanje za različne faktorje $\gamma$. Ko se bližamo $\gamma \to \beta$, epidemija vedno bolj počasi zamira (glej od desne proti levi). Če je $\gamma = \beta$, bi se število bolnih preprosto ustalilo pri nekem ravnovesju.}
    \label{fig:oscillations_A}
\end{figure}

\begin{figure}
    \begin{center}
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{oscillations_B_12d.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{oscillations_B_24d.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{oscillations_B_36d.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{Različno divje oscilacije glede na trajanje imunosti po prebolelosti (zamik $\tau_\mathrm{ID}$). Daljša trajanja imunosti povzročijo oscilatorno obnašanje, ki počasneje izzveni.}
    \label{fig:oscillations_B}
\end{figure}

\begin{figure}
    \begin{center}
        \begin{minipage}{0.42\textwidth}
            \centering
            \includegraphics[width=\textwidth]{oscillations_B_100d_02.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.42\textwidth}
            \centering
            \includegraphics[width=\textwidth]{oscillations_B_100d_03.plot.pdf}
        \end{minipage}%
    \end{center}
    \caption{Poteka za dve zanimivi izbiri parametrov, kjer jasno opazimo valove v $B(t)$, nekoliko podobno poteku dejanske SARS-CoV epidemije. S primerjavo leve in desne slike vidimo tudi, da bolj nalezljiv virus sproži večje število takšnih valov.}
    \label{fig:oscillations_B2}
\end{figure}

Parameter $r$ označuje razmerje med plodnostjo plena in uspešnostjo plenilcev. Majhen $r$ označuje plen, ki se množi relativno počasi, in plenilce, ki so izjemno uspešni v lovi na njih. Lotka-Volterra enačbi pa zdaj zavzameta obliko
\begin{align}
    \dot{\Zed} &= r \left( \Zed - \Zed \Ell \right), \nonumber \\
    \dot{\Ell} &= r^{-1} \left( \Zed\Ell - \Ell \right).
    \label{eq:LV-nondim}
\end{align}
Za ta sistem lahko zapišemo Jakobian (linearizacijo sistema) za Lotka-Volterra sistem~(\ref{eq:LV-nondim})
\begin{equation*}
    J(r, \Zed, \Ell) = \begin{bmatrix}
        r(1 - \Ell) & -r\Zed \\
        r^{-1}\Ell & r^{-1}(\Zed - 1)
    \end{bmatrix}.
\end{equation*}
V stacionarni točki $(\Zed, \Ell) = (0, 0)$ je ta Jakobian
\begin{equation*}
    J(r, 0, 0) = \begin{bmatrix}
        r & 0 \\
        0 & -r^{-1}
    \end{bmatrix}.
\end{equation*}
Vidimo, da sta lastni vrednosti $r$ in $-r^{-1}$ vedno nasprotnih predznakov, torej imamo v tej točki sedlo. Še posebej za majhne $r$ je ob bližanju $(0, 0)$ značilna dinamika skorajšnjega izumrtja, kjer plenilci praktično iztrebijo plen in od stradanja tudi sami skoraj sami izumrejo (slika~\ref{fig:phase} levo), nato pa sledi dolga faza počasne obnove zaloge plena. V faznem diagramu se vedno bolj bližamo t. i. točki izumrtja
\begin{equation*}
    \text{izumrtje: } T^{-} = (0, 0).
\end{equation*}

\begin{figure}
    \begin{center}
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{nondim_phase_01.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{nondim_phase_10.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{nondim_phase_100.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{Fazni diagrami dinamike zajcev in lisic za različne faktorje $r$ (izraz~\ref{eq:r}).}
    \label{fig:phase}
\end{figure}

To je stacionarna točka, kjer izumrejo tako plen kot tudi plenilci ($\Zed = 0$ in $\Ell = 0$, slika~\ref{fig:phase} levo). Če smo točno v $(\Zed, \Ell) = (0, 0)$, nimamo več dinamike, saj sta obe vrsti izumrli. A če ostane le en zajec, se bo dinamika ponovila. Taka stacionarna točka je nestabilna, saj se za nek poljubno majhen $\varepsilon > 0$ začetna točka $T = T^{-} + \varepsilon$ odpelje daleč stran od začetne točka. Za razliko od tega je 
\begin{equation*}
    \text{ravnovesje: } T^{+} = (1, 1),
\end{equation*}
ki označuje ravnovesje populacij plenilca in plena, stabilna stacionarna točka.
\begin{equation}
    \text{ravnovesje: } Z = \frac{\delta}{\gamma}, \quad L = \frac{\alpha}{\beta}.
    \label{eq:ravnovesje}
\end{equation}
Za vsak $\delta > 0$ namreč obstaja tak $\varepsilon > 0$, da bo dinamika, ki jo porodi začetna točka $T = T^{+} + \varepsilon$ ostala v $\delta$ okolici $T^{+}$. Pripadajoč Jakobian je
\begin{equation*}
    J(r, 0, 0) = \begin{bmatrix}
        0 & -r \\
        r^{-1} & 0
    \end{bmatrix},
\end{equation*}
in ker so njegove lastne vrednosti iz $\lambda^2 + 1 = 0$ le imaginarne $\lambda = \pm i$, so po teoriji (glej npr. 4. poglavje v~\cite{ode-theory1979}) trajektorije v okolici te točke cikli podobni krožnicam. To tudi vidimo na slikah~\ref{fig:phase}. Časovne poteke takih trajektorij vidimo v drugi (časovni) obliki na~\ref{fig:by_t}.

\subsection{Odvisnost časovne periode}

Različne trajektorije, ki označujejo različne $r$ oz. različne začetne pogoje imajo različne obhodne čase. Seveda je v analogiji z krožnico pričakovano, da je bodo zaključene trajektorije, ki potekajo dlje od stacionarne točke $(1, 1)$, imele daljši obhodni čas (manjši $\Zed_0$ ob $\Ell_0 = 1$ pomeni večji obhodni čas, glej sliko~\ref{fig:periods}). Poleg tega pa je ta odvisnost mnogo bolj izražena (obhodni časi so večji in predvsem hitreje naraščajo z manjšim $\Zed_0$ ob $\Ell_0 = 1$, spet glej sliko~\ref{fig:periods}).

\subsection{Poseg v okolje, časovno odvisni parametri}

Poglejmo si, kako se sistem odzove na poseg v okolje, npr. večje izobilje hrane za plen. Pri tem se poveča $\alpha$ faktor, kar bomo v brezdimenzijskem sistemu modelirali z časovno odvisnim $A(\tau)$, kot v enačbi
\begin{align}
    \dot{\Zed} &= r \left( A(\tau) \Zed - \Zed \Ell \right), \nonumber \\
    \dot{\Ell} &= r^{-1} \left( \Zed\Ell - \Ell \right).
    \label{eq:LV-nondim-A}
\end{align}
Že iz brezdimenzijskih enot $L_0$ in $Z_0$ (enačba~(\ref{eq:units})), še lepše pa iz faznega diagrama (slika~\ref{fig:equillibrium_shift} levo) se vidi, da bo sprememba $\alpha$ (ali pa $\beta$) nekoliko neintuitivno vplivala le pa populacijo plenilcev. Boljši pogoji za plen se torej prevedejo le na boljše pogoje za plenilce. Obratno bi bolj dolgoživ plenilec pomenil le povečanje populacije plena.

\subsection{Nosilnost okolja za plen in sitost plenilcev}

V model želimo vključiti nekaj dodatnih realističnih obnašanj, ki bi jih lahko pričakovali v takem biološkem sistemu. Prvo upoštevamo končno nosilnost (ang. \textit{carrying capacity}) okolja za populacijo plena $K$. Člen, ki označuje razmnoževanje plena torej dopolnimo kot
\begin{equation*}
    \Zed \to \Zed \left( 1 - \frac{\Zed}{K} \right).
\end{equation*}
Tako se bo za $\Zed \to K$ plen vedno manj učinkovito množil, saj morajo posamezniki (zajci) v populaciji tekmovati med sabo za potrebne vire.
\begin{equation*}
    \Zed\Ell \to \Zed\Ell \left( \frac{1}{1 + \Zed / C} \right),
\end{equation*}
pri čemer smo dodali faktor $\left( \frac{1}{1 + \Zed/C} \right)$, ki se zasiči za $\Zed \ll C$ in tako modelira \textit{diminishing return} vrednost dodatnega plena, ko je plenilec že sit. Lotka-Volterra sistem zdaj dobi obliko
\begin{align}
    \dot{\Zed} &= r \left( \Zed \left( 1 - \frac{\Zed}{K} \right) - \frac{\Zed \Ell}{1 + \Zed / C} \right), \nonumber \\
    \dot{\Ell} &= r^{-1} \left( \frac{\Zed\Ell }{1 + \Zed / C} - \Ell \right).
    \label{eq:LV-improved}
\end{align}
Za tako modificiran Lotka-Volterra sistem imamo namesto dveh stacionarnih točk $(0, 0)$ in $(1, 1)$ zdaj nov set treh stacionarnih točk. Še vedno je prisotna fiksna točka izumrtja $(0, 0)$, prisotna pa je tudi stacionarna točka $(K, 0)$, kjer je populacija zajcev $\Zed = K$ enaka nosilnosti okolja, plenilca pa ni. Za dinamiko sistema pa je pomembna stacionarna točka
\begin{equation}
    B = \left( \frac{C}{C - 1}, \frac{C (KC - K - C)}{K(C-1)^2} \right),
    \label{eq:B}
\end{equation}
ki v grobem prevzame vlogo stacionarne točke ravnovesja (točka $+$ oz. $(1, 1)$, glej enačbo~(\ref{eq:ravnovesje})). A točka $B$ je zdaj odvisna od parametrov $K, C$, dinamika v njeni okolici pa je bolj zanimiva. Zapišemo njen Jakobian
\begin{equation*}
    J_B = J(r, \Zed_B, \Ell_B) = \begin{bmatrix}
        r \left( 1 - \frac{2\Zed}{K} - \frac{C^2 \Ell}{(\Zed + C)^2} \right) & -\frac{rC \Zed}{\Zed + C} \\
        \frac{C^2 \Ell}{r (C + \Zed)^2} & \frac{C\Zed - C - \Zed}{r(C + \Zed)}
    \end{bmatrix}.
\end{equation*}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.95\textwidth]{periods.plot.pdf}
    \end{center}
    \caption{Odvisnost obhodne periode od začetnega pogoja $\Zed_0$, pri tem da je $\Ell_0$ stalno ravnovesni $\Ell_0 = 1$. Za velik $r$ ali $\Zed_0 \to 1^{-}$ so trajektorije v faznem prostoru skoraj krožnice, in obhodni čas je tedaj $T_{2\pi} \to 2\pi$.}
    \label{fig:periods}
\end{figure}


Na podlagi lastnih vrednosti $\lambda_i$ tega Jakobiana bomo karakterizirali obnašanje okoli točke $B$. Glede na rešitve karakterističnega polinoma
\begin{equation}
    p_B(\lambda) = 1 - \frac{1}{C} - \frac{1}{K} + \frac{r(C + C^2 + K - CK)\lambda}{(C-1)CK} + \lambda^2 = 0,
    \label{eq:quadratic}
\end{equation}
ločimo tri možnosti~\cite{ode-theory1979}
\begin{enumerate}
    \item Vse lastne vrednosti vrednosti $\lambda_i \in \mathbb{R}$. Tedaj je točka fiksna točka, njena stabilnost pa je določena s pozitivno definitnostjo Jakobiana. Za kvadratni karakteristični polinom $p_B(\lambda)$ je to ekvivalentno $D > 0$ (izraz~(\ref{eq:disc}), območje narisano z rdečo na sliki~\ref{fig:big} desno spodaj).
    \item Vse lastne vrednosti $\lambda_i \in \mathbb{C}$ in $\Re(\lambda_i) > 0$. Tedaj so trajektorije \textbf{spirale navzven iz fiksne točke}. Ravno tu opazimo limitne cikle, kot je to jasno ilustrirano na sliki~\ref{fig:big} za modro pobarvano območje.
    \item Vse lastne vrednosti $\lambda_i \in \mathbb{C}$ in $\Re(\lambda_i) < 0$. Tedaj so trajektorije \textbf{spirale v fiksno točko}. To je spet jasno ilustrirano na sliki~\ref{fig:big} desno spodaj za nepobarvano območje med rdečim in modrim.
\end{enumerate}
Te tri možnosti bomo ločili z uporabi diskriminante za~(\ref{eq:quadratic})
\begin{equation}
    D = \frac{4CK(C+K - CK) + \frac{r^2}{(C-1)^2} (C + C^2 + K - CK)}{C^2 K^2},
    \label{eq:disc}
\end{equation}
ki jo uprizorimo na sliki~\ref{fig:big} desno spodaj z rdečim območjem $D > 0$. Da ločimo spirale navznoter in spirale navzven bomo uporabili tudi količino, ki je sorazmerna $\Re(\lambda_i)$, sicer
\begin{equation*}
    \Re(\lambda_i) \propto - C- K + CK + C^2,
\end{equation*}
kar narišemo kot modro območje $\Re(\lambda_i) < 0$ na sliki~\ref{fig:big} desno spodaj.

\subsection{Limitni cikli}

Na podlagi diskriminante in kriterija $\Re(\lambda_i) < 0$ lahko zdaj opazimo, kdaj se v faznem poteku pojavijo limitni cikli. Na sliki~\ref{fig:big} so narisani fazni diagrami , t. j. izven območje $D > 0$ in hkrati v območju $\Re(\lambda_i) < 0$.

\begin{figure}
    \begin{center}
        \begin{minipage}{0.33\textwidth}
            \centering
            \includegraphics[width=\textwidth]{nondim1.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.33\textwidth}
            \centering
            \includegraphics[width=\textwidth]{nondim2.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.33\textwidth}
            \centering
            \includegraphics[width=\textwidth]{nondim3.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{Časovni poteki za konkretne začetne pogoje in parametre $r$ iz slik~\ref{fig:phase}. Pri vsakem je zgoraj desno prikazana tudi trajektorija v faznem prostoru.}
    \label{fig:by_t}
\end{figure}

\begin{figure}
    \begin{center}
        \begin{minipage}{0.4\textwidth}
            \centering
            \includegraphics[width=\textwidth]{phase_equillibrium_shift.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.4\textwidth}
            \centering
            \includegraphics[width=\textwidth]{nondim4.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{(Levo) Fazne trajektorije ob časovno odvisnem reproduktivnem faktorju člena $A(\tau) = 1 + \frac{1}{1 + e^{-10(\tau - 5)}}$. Vidimo, da se fiksna točka (ekvilibrijska) zamakne za faktor $A$ proti večji koncentraciji plenilcev. (Desno) Časovni potek za en začetni pogoj.}
    \label{fig:equillibrium_shift}
\end{figure}

\begin{figure}
    \begin{center}
        \begin{minipage}{0.45\textwidth}
            \centering
            \includegraphics[width=\textwidth]{phase_improved.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.45\textwidth}
            \centering
            \includegraphics[width=\textwidth]{phase_spiral.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{Fazna poteka za izboljšan model~(\ref{eq:LV-improved}). (Levo) Približevanje limitnemu ciklu (parametri $(r, K, C) = (0.5, 7, 3)$) za dva začetna pogoja. (Desno) Spiralo približevanje fiksni točki $B$ za dva začetna pogoja (parametri $(r, K, C) = (0.5, 4, 3)$).}
    \label{fig:limit_cycle}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics{big.plot.pdf}
    \end{center}
    \caption{
        Prikaz potekov v faznem prostoru za različne $(K,C)$ in za $r = 0.5$. Označene so fiksne točke $B = B(K, C)$ (enačba~(\ref{eq:B})), kateri se nekateri fazne trajektorije spiralno približujejo, druge pa se ji oddaljujejo. Za tiste, ki se točki $B$ oddaljujejo, opazimo limitne cikle (označeni z rdečo). (Desno spodaj) Označene točke $(K, C)$, za katere smo narisali fazne diagrame. Z rdečo je označeno področje pozitivne diskriminante karakterističnega polinoma~(\ref{eq:quadratic}). Z modro je označeno območje, kjer ima~(\ref{eq:quadratic}) realen del rešitve pozitiven.
    }
    \label{fig:big}
\end{figure}

\begin{figure}
    \begin{center}

    \begin{center}
        \begin{minipage}{0.4\textwidth}
            \centering
            \includegraphics[width=\textwidth]{categorisation.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.4\textwidth}
            \centering
            \includegraphics[width=\textwidth]{categorisation_mean.plot.pdf}
        \end{minipage}
        \begin{minipage}{0.4\textwidth}
            \centering
            \includegraphics[width=\textwidth]{categorisation_dev.plot.pdf}
        \end{minipage}
    \end{center}
    \end{center}
    \caption{}
    \label{fig:categorisation}
\end{figure}

\section{Fotoni v laserju}

Laser opišemo kot snov, sestavljeno iz števila atomov $a$, ki jo stalno vzbujamo, pri tem pa vzbujena stanja $a$ stalno prehajajo nazaj v osnovna stanja s tem, da oddajo fotone svetlobe $f$. To opisuje sistem dveh navadnih diferencialnih enačb
\begin{align}
    \dot{a} &= R - B_2 fa - \beta a, \nonumber \\
    \dot{f} &= -\alpha f + B_1 fa.
    \label{eq:laser}
\end{align}
Ta sistem ima dve fiksni točki. Prva je točka, kjer imamo zgolj atome ($f = 0$), saj vzbujena stanja ustvarjamo ravno dovolj hitro, da kompenziramo razpadanje
\begin{equation*}
    \text{nezadostno črpanje brez fotonov: } \left( \frac{R}{\beta}, 0 \right).
\end{equation*}
Druga kritična točka $(a, f)$ je 
\begin{equation*}
    \text{ravnovesna fiksna točka } C = \left( \frac{B_1 R - \alpha\beta}{B_2 \alpha}, \frac{\alpha}{B_1} \right),
\end{equation*}
na podlagi katere bomo izbrali nove karakteristične dimenzije 
\begin{equation*}
    R_0 = \frac{\alpha\beta}{B_1}, \quad A_0 = \frac{\alpha}{B_1}, \quad F_0 = \frac{\beta}{B_2}, \quad t_0 = \frac{1}{\beta},
\end{equation*}
s katerimi vpeljemo brezdimenzijske količine
\begin{equation*}
    R = R_0 r, \quad
    a = A_0 \mathcal{A}, \quad
    f = F_0 \mathcal{F}, \quad
    t = t_0 \tau,
\end{equation*}
v katerih sistem enačb~(\ref{eq:laser}) ob uvedbi
\begin{equation*}
    q = \frac{\alpha}{\beta},
\end{equation*}
zavzame obliko
\begin{align}
    \dot{\mathcal{A}} = r - \mathcal{F}\mathcal{A} - \mathcal{A}, \nonumber \\
    \dot{\mathcal{F}} = q \left( \mathcal{F}\mathcal{A} - \mathcal{F} \right).
    \label{eq:laser-nondim}
\end{align}

\begin{figure}
    \begin{center}
        \begin{minipage}{0.33\textwidth}
            \centering
            \includegraphics[width=\textwidth]{laser_rsub1.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.33\textwidth}
            \centering
            \includegraphics[width=\textwidth]{laser_rsub3.plot.pdf}
        \end{minipage}
        \begin{minipage}{0.33\textwidth}
            \centering
            \includegraphics[width=\textwidth]{laser_rsub2.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{Fazni poteki sistema~(\ref{eq:laser-nondim}) za $q = 2$ in različne $r$. Slika je narisana tako, da za naključne začetne točke v faznem prostoru enačbo rešujemo naprej, nato pa še nazaj v času. Rešitve v času naprej so narisane v modro-zelenih, časovno obrnjene rešitve pa v oranžno-vijoličnih barvah. Tako je po zelenih barvah takoj vizualno jasno, kam se sistem razvija. (Levo zgoraj) Primer za $r = 5 > 1$. Vidimo, da se sistem razvija k ravnovesnemu $C$, kjer imamo število fotonov $f \propto R$. (Desno zgoraj) Primer za $r = 0.5 < 1$. Za razliko od $r > 1$ se sistem razvija k ravnovesni fiksni točki $(r, 0)$, kjer ni prisotnih nobenih fotonov. (Spodaj) Prehodni primer z $r = 1$.}
    \label{fig:laser_rsub}
\end{figure}

V novih brezdimenzijskih $(\mathcal{A}, \mathcal{F})$ sta fiksni točki podani kot
\begin{align*}
    \text{nezadostno črpanje brez fotonov: } \left( r, 0 \right), \\
    \text{ravnovesna fiksna točka } C = \left( 1, r-1 \right).
\end{align*}
Jakobian v fiksni točki $(r, 0)$ je
\begin{equation*}
    J(r, 0) = \begin{bmatrix}
        -1 & -r \\
        0 & q(r-1)
    \end{bmatrix},
\end{equation*}
za kar preprosto izračunamo diagonalizacijo 
\begin{equation*}
    J(r, 0) = P \begin{bmatrix}
        -1 & 0 \\
        0 & q(r-1)
    \end{bmatrix} P^{-1}, \quad P = \begin{bmatrix}
        1 & 0 \\
        \frac{-r}{1 - q + qr} & 1
    \end{bmatrix},
\end{equation*}
na podlagi katere lahko za fiksno točko ločimo naslednje možnosti:
\begin{enumerate}
    \item Če je $r > 1$, je $(r, 0)$ sedlo in nestabilna fiksna točka sistema~(\ref{eq:laser-nondim}). Glej sliko~\ref{fig:laser_rsub} levo.
    \item Če je $r < 1$, je $(r, 0)$ stabilna fiksna točka sistema~(\ref{eq:laser-nondim}). Glej sliko~\ref{fig:laser_rsub} sredinsko.
    \item Če je $r = 1$, je $(1, 0)$ sedlo za Gaussovsko ukrivljenostjo 0. Tedaj je tudi druga fiksna točka $C$ ista točka. Glej sliko~\ref{fig:laser_rsub} desno.
\end{enumerate}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.8\textwidth]{laser_qset.plot.pdf}
        \includegraphics[width=0.8\textwidth]{laser_qset2.plot.pdf}
    \end{center}
    \caption{(Zgoraj) Poteka za različne $q$ pri $r = 1.5$ in $r = 5$ in pri začetnemu pogoju $(\mathcal{A}_0, \mathcal{F}_0) = (3, 10^{-6})$. Tak potek predstavlja vklop laserja, kjer začnemo z majhno populacijo fotonov. V modri, črni in rdeči barvi so narisani poteki za \textit{underdamped}, \textit{critically damped} in \textit{overdamped} potek, ki ustreza $q$ vrednostim okoli kritičnega $q = 1$. To lahko vidimo tudi na sliki~\ref{fig:rsub2}. (Spodaj) Poteka za različne $q$ pri $r = 1.5$ in $r = 5$ in pri začetnemu pogoju $(\mathcal{A}_0, \mathcal{F}_0) = (0, 5)$.}
    \label{fig:qset}
\end{figure}

Iz slike~\ref{fig:laser_rsub} lahko vidimo, da ima sistem v točkah $(r, 0)$ in točki $C$ dve ravnovesji. Kateremu ravnovesju se približuje v $t \to \infty$ pa je odvisno predvsem od moči črpanja. Za črpanja $r > 1$ se bo laser približeval točki $C$, v kateri imamo neko število fotonov $\mathcal{F} = r-1$. Potek približevanja temu ravnovesju za različne $q$ lahko vidimo na sliki~\ref{fig:qset}. Nasprotno bo za šibko črpanje $r < 1$ pa bo populacija fotonov ne glede na začetno zamrla proti 0 (npr. slika~\ref{fig:laser_rsub}).

To lahko vidimo tudi iz analize fiksne točke $C = (1, r-1)$, v kateri je karakteristični polinom Jakobiana 
\begin{equation*}
    \lambda^2 + r\lambda + qr - q = 0,
\end{equation*}
iz česar lahko določimo lastni vrednosti
\begin{equation*}
    \lambda_{1,2} = -\frac{r}{2} \pm \sqrt{q - qr + \frac{q^2}{4}}.
\end{equation*}
Prvo predpostavimo, da je diskriminanta v tej rešitvi pozitivna. Če zapišemo diskriminanto pod korenom kot $r^2 - q(r-1)$, je jasno, da velja:
\begin{enumerate}
    \item Za $r > 1$ sta $\lambda_{1,2} < 0$ in je $C$ stabilna fiksna točka. Glej sliko~\ref{fig:laser_rsub} levo.
    \item Za $r < 1$ sta $\lambda_{1} > 0$, $\lambda_{2} < 0$ in je $C$ nestabilna fiksna točka. Glej sliko~\ref{fig:laser_rsub} sredinsko.
    \item Za $r = 1$ imamo zopet degenerirani primer, kjer sta obe fiksni točki enaki (slika~\ref{fig:laser_rsub} desno).
\end{enumerate}
Kako pa interpretiramo Jakobian, če je diskriminanta negativna, t. j. da je
\begin{equation*}
    q > \frac{r^2}{4(r-1)}.
\end{equation*}
Tedaj imamo lastni vrednosti z $\Re(\lambda_{1,2}) < 0$ in neničelnim imaginarnim delom $\Im(\lambda_{1,2}) \ne 0$. Taka lastna vrednost narekuje, se trajektorije točki $C$ približujejo spiralno, ne več kot trajektorije ki so asimptotsko vzporedne z enim od lastnih vektorjev Jakobiana v $C$. Ta sprememba je vidna na sliki~\ref{fig:rsub2}.

\begin{figure}
    \begin{center}
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{laser_rsub_S4.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{laser_rsub_S1.plot.pdf}
        \end{minipage}
        \begin{minipage}{0.32\textwidth}
            \centering
            \includegraphics[width=\textwidth]{laser_rsub_S2.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{Vpliv različnih $q$ na fazne poteke pri $r =5$. Pri $q = 1$ se zgodi prehod med $q > 1$ režimom (Desno), kjer overshootamo in iznihamo v ravnovesje in režimom $q < 1$ (Levo), kjer nihanja sploh ni (nekakšno podkritično dušeno, \textit{underdamped} obnašanje).}
    \label{fig:rsub2}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.8\textwidth]{laser_rset.plot.pdf}
    \end{center}
    \caption{Časovni poteki za $q = 3$ in $q = 8$ in različne moči črpanja $r$. Vidimo, kako se spreminja ravnovesna vrednost $\mathcal(\tau \to \infty) = r - 1$.}
    \label{fig:rset}
\end{figure}

\begin{figure}
    \begin{center}
        \begin{minipage}{0.4\textwidth}
            \centering
            \includegraphics[width=\textwidth]{envelope.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.52\textwidth}
            \centering
            \includegraphics{beats_by_qr.plot.pdf}
        \end{minipage}
    \end{center}a
    \caption{
        Analiza nihanj ob vklopu laserja. (Levo) V črni vidimo za $(q, r) = (10, 2)$ funkcijo $\widetilde{\mathcal{F}}(\tau) \das \mathcal{F}(\tau) - r + 1$, ki opisuje tranzitivni presežek nad ravnovesno vrednost $r - 1$. To funkcijo nato razstavimo preko Hilbertove
        transformacije $\mathcal{H}$ na oscilatorno fazo in ovojnico (magnitudo). (Desno zgoraj) Za parametra $(q, r)$ prešteto število $6\pi$ skokov v fazi $\mathcal{H}(\widetilde{\mathcal{F}}(\tau))$. (Desno spodaj) Za parametra $(q, r)$ povprečna perioda med posameznimi $6\pi$ skoki v fazi $\mathcal{H}(\widetilde{\mathcal{F}}(\tau))$.
    }
    \label{fig:envelope}
\end{figure}


\nocite{*}
% \emergencystretch=1em
\printbibliography

\end{document}
