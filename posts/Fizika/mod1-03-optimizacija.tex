% !TEX program = xelatex

\documentclass{article}
\usepackage[a4paper,margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{lit.bib}

% Images and hyperlinks
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathrsfs}
\usepackage{float}

% Custom definitions
% ...
% Differential
\newcommand{\diff}{\mathrm{d}}
% Listings
\usepackage{listings}
\usepackage{xcolor}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    numberstyle=\tiny\color{codegray},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    xleftmargin=3em,
    aboveskip=1.5em,
    belowskip=1.5em,
    abovecaptionskip=1em
}
\lstset{style=mystyle}
\renewcommand{\lstlistingname}{Izsek}

\title{
\sc\large Modelska analiza I\\
\bigskip
\bf\Large 3.~naloga: Numerična optimizacija
}
\author{Martin Šifrar, 28242021}

\begin{document}
\maketitle

\section{Navodila}

\begin{enumerate}
    \item Thomsonov problem: na prevodno kroglo nanesemo N enakih (klasičnih) nabojev. Kako se razmestijo po površini?1 Zahtevamo seveda minimum elektrostatične energije. Primerjaj učinkovitost in natančnost za različne minimizacijske metode, npr. Powellovo ali n-dimenzionalni simpleks (amebo oz. Nelder-Mead).

    \item Problem optimalne vožnje skozi semafor, ki smo ga spoznali pri nalogi 1, lahko rešujemo tudi z numerično minimizacijo, če časovno skalo diskretiziramo. Lagrangianu $\int \dot{v} \,\diff{t} − \lambda \int v \,\diff{t}$ lahko dodamo omejitev hitrosti v obliki členov $\mathop{\mathrm{exp}}(\beta(u − u_\text{lim}))$, če hočemo (približno) zagotoviti $u \le u_\text{lim}$. Izpolnitev pogoja je toliko ostrejša, kolikor večji $\beta$ vzamemo. Poskusiš lahko tudi druge omejitvene funkcije, na primer kakšno funkcijo s polom. Za iskanje Lagrangeovega multiplikatorja lahko uporabiš bisekcijo ali kakšno drugo vgrajeno metodo za iskanje ničel na funkciji $l(\lambda) = \int v(\lambda, t) \,\diff{t}$, kjer je $v(\lambda, t)$ rezultat minimizacije pri izbranem $\lambda$. V tem primeru je enakost izpolnjena eksaktno.
\end{enumerate}

\section{Thomsonov problem}

Rešujemo nelinearni minimizacijski, kjer minimiziramo potencialno energijo
\begin{equation}
    V = \sum_{j < i} \frac{1}{|\mathbf{r}_j - \mathbf{r}_i|},
    \label{eq:V}
\end{equation}
pri tem pa sta $\mathbf{r}_i$ in $\mathbf{r}_j$ omejena na površino krogle in parametrizirana z enim parom $(\vartheta, \phi)$, tako da je
\begin{equation*}
    \mathbf{r} = \begin{pmatrix} \cos{\phi} \sin{\vartheta} & \sin{\phi} \sin{\vartheta} & \cos{\vartheta} \end{pmatrix}.
\end{equation*}
Konfiguracije, ki jih dobimo in njihove konveksne ovojnice (pridobljene z \texttt{scipy.spatial.ConvexHull}), vidimo prikazane na sliki~(\ref{fig:pts}).

\begin{figure}
    \begin{center}
    \includegraphics[width=0.9\textwidth]{thompson_pts.plot.png}
    \end{center}
    \caption{Konfiguracije nabojev, ki minimizirajo potencialno energijo na površini krogle.}
    \label{fig:pts}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics{energy_by_N.plot.pdf}
    \end{center}
    \caption{(Levo) Naraščanje skupne potencialne energije za rešitve Thomsonovega problema z $N$ naboji. (Sredinsko) Skupna potencialna energija na enoto parov (parov je $\sim N^2$). Rdeče črtkano je prikazana zvezna limita samo-interakcije enakomerno nabite sfere. (Desno) Odvisnost minimalne razdalje med naboji v odvisnosti od $N$.}
    \label{fig:energy_by_N}
\end{figure}

\subsection{Naboji na površini torusa}

Podoben problem minimizacije potencialne energije~(\ref{eq:V}) bomo rešili še za naboje na površini torusa, spet parametriziranega z $(\vartheta, \phi)$ kot
\begin{equation*}
    \mathbf{r} = \begin{pmatrix} (1 + \rho \cos\vartheta) \cos\phi & (1 + \rho \cos\vartheta) \sin\phi & \rho \sin\vartheta \end{pmatrix}.
\end{equation*}
Dobljene konfiguracije za $\rho = 1$ torus narišemo na slikah~\ref{fig:pts_torus} in~\ref{fig:pts_torus_side}. Za $N <= 10$ so naboji razporejeni po zunanjem obodu torusa z radijem $1 + \rho$. Za večje $N$ so razporeditve presenetljivo nesimetrične (slika~\ref{fig:pts_torus_side}), izjema je le razporeditev za $N = 24$, ki je edina zares simetrična glede na preslikavo čez veliko os torusa.

\begin{figure}
    \begin{center}
    \includegraphics[width=0.9\textwidth]{thompson_torus.plot.png}
    \end{center}
    \caption{Konfiguracije nabojev, ki minimizirajo potencialno energijo na površini torusa z $\rho = 1/4$.}
    \label{fig:pts_torus}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics[width=0.9\textwidth]{thompson_torus_side.9.plot.png}
    \includegraphics[width=0.9\textwidth]{thompson_torus_side.20.plot.png}
    \end{center}
    \caption{Stranski pogled, naboji na površini torusa z $\rho = 1/4$.}
    \label{fig:pts_torus_side}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics{torus_energy_by_N.plot.pdf}
    \end{center}
    \caption{(Levo) Naraščanje skupne potencialne energije za rešitve Thomsonovega problema na torusu z $N$ naboji. (Sredinsko) Skupna potencialna energija na enoto parov (parov je $\sim N^2$). (Desno) Odvisnost minimalne razdalje med naboji v odvisnosti od $N$.}
    \label{fig:torus_energy_by_N}
\end{figure}

\begin{figure}[ht]
    \begin{center}
    \includegraphics{torus_energy_by_rho.plot.pdf}
    \end{center}
    \caption{Spreminjanje malega radija torusa $\rho$. (Levo) Odvisnost potencialne energije od malega radija za različne $N$. (Desno) Odvisnost najmanjše razdalje med naboji.}
    \label{fig:torus_energy_by_rho}
\end{figure}

\subsection{Naboji na površini kocke}

Še ena površina, za katero je precej lepo reševati problem porazdelitve nabojev, je kocka. Namesto dejanske kocke bomo vzeli t. i. super-sfero
\begin{equation}
    x^{2p} + y^{2p} + z^{2p} = 1, \quad p \in \mathbb{N},
\end{equation}
pri čemer izberemo $p$ dovolj velik, da je kocka \blockquote{ostra}. Konkretno izberemo $p = 200$. Površino prav tako kot pri krogli parametriziramo s kotoma $(\theta, \phi)$, le da za radij velja
\begin{equation*}
    r(\vartheta, \phi) = \left( \frac{1}{ \cos^{2p}\vartheta + (\cos\phi \sin\vartheta)^{2p} + (\sin\phi \sin\vartheta)^{2p} } \right)^\frac{1}{2p}.
\end{equation*}
Dobljene porazdelitve nabojev narišemo na slikah~\ref{fig:cube1} in~\ref{fig:cube2}.

\begin{figure}
    \begin{center}
    \includegraphics[width=0.9\textwidth]{thompson_hypersphere.1.plot.png}
    \end{center}
    \caption{Konfiguracije nabojev, ki minimizirajo potencialno energijo na površini kocke.}
    \label{fig:cube1}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics[width=0.9\textwidth]{thompson_hypersphere.2.plot.png}
    \end{center}
    \caption{Še nekaj konfiguracij nabojev, ki minimizirajo potencialno energijo na površini kocke.}
    \label{fig:cube2}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics{cube_energy_by_N.plot.pdf}
    \end{center}
    \caption{(Levo) Naraščanje skupne potencialne energije za rešitve Thomsonovega problema na kocki z $N$ naboji. (Sredinsko) Skupna potencialna energija na enoto parov (parov je $\sim N^2$). (Desno) Odvisnost minimalne razdalje med naboji v odvisnosti od $N$. Vidimo, da je za razliko od torusa in sfere padanje minimalne razdalje med naboji bolj stopničasto. To se sklada s konfiguracijami, ki jih vidimo na~\ref{fig:cube1} in~\ref{fig:cube2}.}
    \label{fig:cube_energy_by_N}
\end{figure}

\subsection{Izogibanje lokalnim maksimumom} \label{sec:local-extrema}

V namen izogibanja lokalnim maksimumom sem uporabil zelo preprost postopek. Prvo sem metodo zagnal za neke (tipično naključne začetne pogoje), nato pa sem optimizacijo pognal še 10-krat, z začetnimi parametri, ki sem jih dobil kot vsoto dobljenega prvega maksimuma in $\sigma = 1$ ali $\sigma = 5$ Gaussovskega šuma. Tako sem se izognil nekaterim lokalnim maksimumom, a da sem jim zares ušel, ne morem trditi.

\subsection{Energije in zvezna limita}

Preverimo lahko tudi zvezno limito Thomsonovega problema. Komulativna interakcija vsakega izmed delčkov $\diff S$ električno nabite krogle $R = 1$ z ostalimi naboji te krogle (površinska gostota $\sigma = \frac{N}{4\pi}$) je
\begin{equation*}
    V = 4\pi \sigma \int_{4\pi} \frac{\sigma\,\diff S(\mathbf{r})}{|\mathbf{r} - \mathbf{r_0}|} = \frac{N^2}{2},
\end{equation*}
kar ustreza tudi našemu diskretnemu problemu, ko gre $N \to \infty$ (glej sredinsko sliko~\ref{fig:energy_by_N}).

Tudi potencialna energija konfiguracij na torusu (slika~\ref{fig:torus_energy_by_N}) ima podobno obliko. Iizgleda, da se približuje $0.5$. Za kocko (slika~\ref{fig:cube_energy_by_N}) pa je vrednost stalno nekoliko nižja in se vizualno približuje neko vrednosti med $3$ in $4$.

\subsection{Natančnost in časovna zahtevnost}

Natančnost algoritma lahko ocenimo s tem, da preverimo odstopanja izračunanih minimalnih potencialnih energij od referenčnih vrednosti iz~\cite{wiki-thomson}. Na sliki~\ref{fig:err_by_N} vidimo odstopanja za prvih 32 nabojev za različne metode in dva različna začetna pogoja. Vidimo, da sta metodi BFGS in CG znatno boljši od drugih dveh. Nelder-Mead (simplex) in Powellova metoda v SciPy vmesniku vrneta opozorilo:

\medskip
\noindent
\texttt{Warning: Maximum number of function evalutaions has been exceeded.}
\medskip

\noindent
Tudi če za faktor 10 povečamo število dovoljenih vrednotenj funkcije (sorazmerno z $N$), to izboljša rezultat Nelder-Mead in Powellove metode le za par naslednjih $N$. Ti dve metodi torej zelo hitro rastete v časovni zahtevnosti v odvisnosti od $N$. Časovno zahtevnost si bolj natančno pogledamo na slikah~\ref{fig:perf_by_N},~\ref{fig:perf_by_N.martin-debian}. Vidimo, da sta metodi BFGS in CG precej hitrejši od Nelder-Mead (simplex) metode in Powellove metode. Za večje $N$ (npr. $N > 200$) pa se nam splača problem pognati tudi na GPU enoti (glej sliko~\ref{fig:perf_by_N.martin-debian}).

\subsection{Različni začetni pogoji}

Poskusimo tudi, ali lahko pravilna izbira začetnih pogojev pospeši reševanje problema. Primerjali bomo začetno naključno in Fibonaccijevo razporeditev
\begin{equation*}
    (\vartheta, \phi) = \left( \frac{i}{\varphi}, \frac{i + 1/2}{N} \right), i = 0, 1, \dots, N,
\end{equation*}
pri čemer je $\phi \approx 1.61803$. Na sliki~\ref{fig:fib_rand} vidimo, da je potencialna energija $V$ stalno manjša pri Fiboneccijevi kot pri naključni razporeditvi. Zato bi torej pričakovali, da je tak začetni pogoj bližje rešitvi Thomsonovega problema in bo kot tak hitreje konvergiral k dejanski rešitvi. To preverimo z dejanskim vrednotenjem na sliki~\ref{fig:perf_by_N}, kjer narišemo čase izvajanja in števila evaluacij $V$ v odvisnosti od števila delcev. Od metod se najboljše izkažeta metodi BFGS in CG, primerjava med začetnimi pogoji pa pokaže (slika~\ref{fig:perf_by_N} desno), da so naključni začetni pogoji na primeru BFGS metode v mediani okoli $44\%$ slabši od Fiboneccijevih v času konvergence in $21\%$ slabši v števila evaluacij $V$. Torej se nam v večini primerov splača vzeti Fiboneccijeve začetne pogoje.

\begin{figure}
    \begin{center}
    \includegraphics{err_by_N.plot.pdf}
    \end{center}
    \caption{Odstopanja prvih 32 minimalnih energij od referenčnih~\cite{wiki-thomson}. Vidimo, da sta metodi BFGS in CG znatno boljši od Nelder-Mead (simplex) metode in Powell metode.}
    \label{fig:err_by_N}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.9\textwidth]{fib_pts.plot.png}
        \includegraphics[width=0.9\textwidth]{random_pts.plot.png}
    \end{center}
    \caption{Čas izvajanja minimizacije. (Zgoraj) Fiboneccijev začetni pogoj. (Spodaj) En primer naključnih začetnih pogojev.}
    \label{fig:fib_rand}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics{perf_by_N.plot.pdf}
    \end{center}
    \caption{(Levo zgoraj) Čas izvajanja za reševanje Thomsonovega problema z različnimi metodami in z Fibonaccijevimi začetnimi pogoji (slika~\ref{fig:fib_rand} zgoraj). (Levo spodaj) Število evaluacij minimizirane funkcije $V$. (Desno, zgoraj in spodaj) Razmerje časov izvajanja in števila evaluacij za naključne začetne pogoje v primerjavi z Fibonaccijevimi začetnimi pogoji. Rdeči črti prikazujeta mediano razmerij med naključnimi in Fibonaccijevimi začetnimi pogoji. To da je mediana $> 1$ pomeni, da se nam splača delati z Fibonaccijevimi začetnimi pogoji.}
    \label{fig:perf_by_N}
\end{figure}

\begin{figure}
    \begin{center}
    \includegraphics{perf_by_N.martin-debian.plot.pdf}
    \end{center}
    \caption{Časi izvajanja na drugem računalniku (z GPU enoto), kjer preizkusimo še \texttt{jaxopt} metode iz istoimenske knjižnice. Te metodi se izvajata na GPU enoto in odraža veliko manj izrazito rast kot CPU metode iz SciPy. Za $N = 200$ in več sta precej hitrejši od metod, ki se izvajata na CPU. Hkrati pa sta manj primerni za majhne $N$, kjer še vedno vidimo overhead priprave in prenašanja podatkov na GPU.}
    \label{fig:perf_by_N.martin-debian}
\end{figure}

\section{Optimalna vožnja skozi semaforje}

Zdaj bomo kot minimizacijski problem rešili še 1. nalogo optimalne vožnje do semaforja\footnote{Za navodilo in splošno formulacijo problema glej nalogo 101.}. Prvo potek $w(\tau)$ diskretiziramo na mreži
\begin{equation*}
    w_i = w(\tau_i), \quad \tau_i = \frac{1}{N_\tau}, \frac{2}{N_\tau}, \dots, \frac{N_\tau}{N_\tau}.
\end{equation*}
Pravzaprav ne potrebujemo povsem splošnih metod minimizacije, temveč je dovolj njegov posebni primer, t. i. kvadratično programiranje, kjer je kriterij za minimizacijo oblike
\begin{equation*}
    c = \mathbf{q}^T \mathbf{x} + \mathbf{x}^T H \mathbf{x},
\end{equation*}
pri čemer je matrika $H$ neka pozitivno semi-definitna matrika, t. j. kvadratna forma $\mathbf{x}^T H \mathbf{x}$ predstavlja nek minimum, ne maksimuma ali sedla. Za optimizacijo kumulativnega kvadrata pospeška prvo določimo pospeške preko končnih diferenc (\textit{forward} diferenca 1. reda). V matričnem zapisu
\begin{equation}
    \dot{\mathbf{w}} =
    \begin{bmatrix}
        \dot{w}_1 \\
        \dot{w}_2 \\
        \dot{w}_3 \\
        \vdots
    \end{bmatrix}
    \approx
    N \begin{bmatrix}
        w_2 - w_1  \\
        w_3 - w_2  \\
        w_4 - w_3  \\
        \vdots
    \end{bmatrix}
    =
    N \begin{bmatrix}
        -1 & 1 \\
        0 & -1 & 1 \\
        & 0 & \ddots & \ddots \\
        & & \ddots & \ddots & 1 \\
         & & & 0 & -1 & 1
    \end{bmatrix}
    \begin{bmatrix}
        w_1 \\
        w_2 \\
        w_3 \\
        \vdots
    \end{bmatrix}
    = D_1 \mathbf{w}
    \label{eq:matrix}
\end{equation}
Če želimo čim manjši kumulativni kvadrat pospeška, je optimizacijski kriterij
\begin{equation}
    c = \dot{\mathbf{w}}^T \dot{\mathbf{w}} \approx (D_1 \mathbf{w})^T D_1 \mathbf{w} = \mathbf{v}^T H_1 \mathbf{w},
    \label{eq:c-qp}
\end{equation}
pri čemer je Hessova matrika iz same definicije pozitivno semi-pozitivna
\begin{equation*}
    H_1 = D_1^T D_1
    =
    N^2 \begin{bmatrix}
        1 & -1 \\
        -1 & 2 & -1 \\
        & -1 & 2 & -1 \\
        & & -1 & \ddots & \ddots \\
        & & & \ddots & \ddots & -1 \\
        & & & & -1 & 1 \\
    \end{bmatrix},
\end{equation*}
in je precej podobna matriki končnih diferenc za $-\ddot{w}$, razen v zgornjem levem in spodnjem desnem kotu. Pri optimizaciji zahtevamo, da semafor prevozimo ob času $\tau = 1$, kar ustreza linearni vezi
\begin{equation}
    1 = \int_0^1 w \,\diff\tau \approx \frac{1}{N} \left( w_1 + w_2 + \dots + w_N \right).
    \label{eq:vez-0}
\end{equation}
To vez bi lahko upoštevali tako, kot je predlagano v navodilu naloge, npr. s strelsko metodo na rešitvah za nek $\lambda$. Sam sem izbral preprostejši način upoštevanja te vezi. Sicer se gre za to, da lahko na podlagi vezi~(\ref{eq:vez-0}) eno spremenljivko kar odstranimo iz funkcijske oblike optimizacijskega kriterija
\begin{equation*}
    c(w_1, w_2, \dots, w_{N_\tau}) = c(w_2, \dots, w_{N_\tau}),
\end{equation*}
saj lahko preko vezi izrazimo $w_1 = f(w_2, w_3, \dots)$. Ugotovil sem, da je v implementaciji zares dovolj, da take odvečne prostostne le \blockquote{skrijemo} iz funkcijske odvisnosti kriterija, ne tudi odstranimo iz prostora parametrov $\mathbf{w}$, v katerem iščemo optimalno rešitev. V kodi to izgleda kot:

\begin{lstlisting}[language=Python, caption=Optimizacijski kriterij.]
def regularize(w_vals, *args):
    num, w0, w1, _, _, _, fixed_w1, _ = args
    w_vals[0] = w0
    if fixed_w1:
        w_vals[num - 1] = w1

    w_vals[1] = (1 - 1 / num * (w_vals[0] + np.sum(w_vals[2:]))) * num
    return w_vals


def cost_semafor_nonlinear(w_vals, *args):
    _, _, _, w_max, β, edge_type, _, FD1 = args
    w_vals = regularize(w_vals, *args)
    dot_w = FD1 @ w_vals
\end{lstlisting}

\subsection{Primerjava med splošno in kvadratično optimizacijo}

Za optimizacijski kriterij komulativnega pospeška~(\ref{eq:c-qp}) lahko brez težave uporabimo splošne metode nelinearne optimizacije, kot smo to počeli za Thomsonov problem. A kot bomo videli imajo metode za kvadratično optimizacijo mnogo prednosti.

Za kvadratično optimizacijo oz. t. i. kvadratično programiranje z lahkoto izrazimo vezi kot je npr. omejitev maksimalne hitrosti. Splošne nelinearne optimizacijske metode pa tipično delujejo v režimu brez vezi. Vezi moramo tedaj dodati našemu kriteriju kot
\begin{equation*}
    c^{(z vezmi)}(\mathbf{w}) = c(\mathbf{w}) + G(\mathbf{w}),
\end{equation*}
pri čemer je $G(\mathbf{w})$ funkcija, ki zavzema veliko (idealizirano neomejeno) vrednost izven dovoljenega območja, ki ga določa vez. Če bi torej za metodo kvadratičnega programiranja zahtevali po elementih
\begin{equation*}
    \mathbf{w} \le w_\text{max},
\end{equation*}
moramo za splošne nelinearne metode dodati kriteriju nek $G$. Poskusili in primerjali bomo $G$ funkcije oblik
\begin{align}
    G_0(\mathbf{w}) &= \sum_i 10^6 \cdot \theta(w_i - w_\text{max}), \label{eq:g0} \\
    G_1(\mathbf{w}) &= \sum_i e^{\beta (w_i - w_\text{max})}, \label{eq:g1} \\
    G_2(\mathbf{w}) &= \sum_i \mathop{\text{max}} \left ( 0, e^{\beta (w_i - w_\text{max}) - 1} \right),
    \label{eq:g2}
\end{align}
pri čemer je $\theta(x)$ Heaviside koračna funkcija. Te funkcije primerjamo na sliki~\ref{fig:G}, kjer vidimo, da se lahko obnašajo precej patološko. Še najlepše se obnaša $G = G_2$ za $\beta = 1$, saj kar sovpada za QP rešitvijo.
\begin{figure}
    \begin{center}
        \begin{minipage}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{semafor_nonlin_beta.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{semafor_nonlin_step.plot.pdf}
        \end{minipage}
    \end{center}
    \begin{center}
        \includegraphics[width=0.5\textwidth]{semafor_nonlin_beta2.plot.pdf}
    \end{center}
    \caption{Primerjava oblik funkcije $G$. Črtkano črno je narisana rešitev, pridobljena z kvadratičnim programiranjem (ang. \textit{QP}). (Levo zgoraj) Koračna oblika $G = G_0$, kot v~(\ref{eq:g0}). (Desno zgoraj) Eksponentna oblika $G = G_1$, kot v~(\ref{eq:g1}). (Spodaj) Rektificirana eksponentna oblika $G = G_2$, kot v~(\ref{eq:g2}).}
    \label{fig:G}
\end{figure}

Poleg tega, da za kvadratno programiranje ne potrebujemo izbirati oblike funkcije $G$, je metoda tudi dosti hitrejša. Na sliki~\ref{fig:perf_semafor}  vidimo, da so QP metode hitrejše kar za velikostni razred do dva, hkrati pa sem v lastnih eksperimentih spoznal, da so QP metode tudi dosti robustnejše.

\subsection{Več omejitev za vožnjo skozi semafor}

Ko problem optimalne vožnje rešujemo v diskretizirani obliki, imamo na voljo precej bogat nabor možnih omejitev za naše rešitve. Te pridejo v problem v direktno v obliki vezi, npr. $\mathbf{w} \le w_\text{max}$, ali pa odstopanje od meje te vezi dodamo našemu minimizacijskemu kriteriju v obliki $G(\mathbf{w})$ člena, ki kaznuje rešitve izven dovoljenega območja. Kar v obliki seznama bom navedel vse takšne omejitve, ki sem jih implementiral za model optimalne vožnje. Ker sem se zaradi hitrosti lažje igral z QP metodo, bom opisal kako sem jih implementiral v tem kontekstu. Prevod za uporabo z nelinearnimi metodami je preprost in sledi zgledu za omejitev maksimalne hitrosti iz slike~\ref{fig:G}.

\begin{enumerate}
    \item Fiksiranje začetne $w_0$
    \item Fiksiranje končne $w(1) = w_1$ hitrosti.
    \item Vez opravljene poti $s(1) = 1$ do semaforja~(izraz (\ref{eq:vez-0})).
    \item Omejitev maksimalne hitrosti $\mathbf{w} \le w_\text{max}$ (glej sliko~\ref{fig:wmax}).
    \item Prepoved negativnih hitrosti $\mathbf{w} \ge 0$. To prepreči \textit{overshoot} čez semafor, saj je pot potem monotona funkcija (slika~\ref{fig:w1} spodaj levo).
    \item \label{item:nonover} Manj stroga omejitev za preprečevanja \textit{overshoota} čez semafor pred $\tau = 1$, ki za vsak $i = 1, 2, \dots, N$ zahteva $\int_0^{\tau_i} w\,\diff \tau \le 1$ (slika~\ref{fig:w1} spodaj desno).
    \item Poljubni semaforji, postavljeni za $k = 1, 2, \dots, N_\text{sem}$, pri čemer $(\tau_k, s_k)$ določata položaj semaforja in čas prižiga semaforja. Za vsak semafor $(\tau_k, s_k)$ izberemo nek $\tau_i$ iz osnovne diskretizacije, ki je najbližje $\tau_k$, nato pa zahtevamo $\int_0^{\tau_i \approx \tau_k} w\,\diff \tau = s_k$. Na slikah~\ref{fig:multi} vidimo primer 5 semaforjev, postavljenih v nekem neenakomernem zaporedju.
    \item Omejitev za preprečevanje \textit{overshoota} za semaforje $(\tau_k, s_k)$. Gre se le za omejitev~\ref{item:nonover}, le ponovljeno za vsak končni čas $\tau = \tau_k$, ne le $\tau = 1$. To omejitev vidimo na primeru 5 semaforjev na sliki~\ref{fig:multi} spodaj desno.
\end{enumerate}

\begin{figure}
    \begin{center}
        \includegraphics{semafor_perf_by_Nt.plot.pdf}
    \end{center}
    \caption{Primerjava časov izvajanja za diskretiziran problem optimalne vožnje. Primerjavo hitrejša izmed nelinearnih splošnih algoritmov (\texttt{BFGS} in \texttt{CG}) ter algoritme kvadratičnega programiranja (\texttt{cvxopt}, \texttt{daqp}, \texttt{ecos}, \texttt{scs}, \texttt{qpax}, \texttt{quadprog}). Kvadratično programiranje za tak tip problema omogoča mnogo hitrejše reševanje.}
    \label{fig:perf_semafor}
\end{figure}

\begin{figure}
    \begin{center}
        \begin{minipage}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{no-wmax.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{wmax.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{Optimalen potek hitrosti za $N_\tau = 200$ in različne začetne hitrosti $w_0$. (Levo) Brez omejitve hitrosti. (Desno) Z omejitvijo maksimalne hitrosti $w_\text{max} = 1.2$.}
    \label{fig:wmax}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.5\textwidth]{w1.plot.pdf}
    \end{center}
    \begin{center}
        \begin{minipage}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{w1-nonneg.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{w1-nonover.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{Optimalen potek hitrosti za $N_\tau = 200$ z začetno hitrostjo $w_0 = 10$ in različne $w(1) = w_1$. (Zgoraj) Brez dodatnih omejitev, le $w_0$, $w_1$ in vez~(\ref{eq:vez-0}). (Levo spodaj) Vez, ki prepoveduje negativne hitrosti $\mathbf{w} \ge 0$. (Desno spodaj) Upoštevana vez, ki zahteva $s(\tau) < 1$ za $\tau \in [0, 1)$, torej preprečuje \textit{overshoot} semaforja pred časom $\tau = 1$.}
    \label{fig:w1}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.5\textwidth]{multi.plot.pdf}
    \end{center}
    \begin{center}
        \begin{minipage}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{multi-nonneg.plot.pdf}
        \end{minipage}%
        \begin{minipage}{0.5\textwidth}
            \centering
            \includegraphics[width=\textwidth]{multi-nonover.plot.pdf}
        \end{minipage}
    \end{center}
    \caption{Optimalen potek hitrosti za $N_\tau = 200$ in 5 neenakomerno postavljenih semaforjev. Končna hitrost pri zadnjem semaforju je $w_1 = 0$ in rišemo različne $w_0$. (Zgoraj) Brez \textit{overshoot} omejitev, le omejitev maksimalne hitrosti $w_\text{max} = 3.5$, fiksirana $w_0$, $w_1$ in vez~(\ref{eq:vez-0}). Lepo vidimo, kako je za lovljenje 4. semaforja potrebno čim hitreje pospešiti na omejitev. (Levo spodaj) Vez, ki prepoveduje negativne hitrosti $\mathbf{w} \ge 0$. To povsem prepreči vzvratno vožnjo in povzroči odrezano obliko funkcije med $\tau \sim 0.65$ in $\tau \sim 0.8$. (Desno spodaj) Upoštevana vez, ki zahteva $s(\tau) < s_i$ za $\tau \in [0, \tau_i)$, pri čemer $(\tau_i, s_i)$ določata položaj semaforja in čas prižiga semaforja. Ta vez torej preprečuje \textit{overshoot} semaforja pred časom prižiga.}
    \label{fig:multi}
\end{figure}

\nocite{*}
\emergencystretch=1em
\printbibliography

\end{document}
