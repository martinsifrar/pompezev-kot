<figure id="fig:video">
  <video width="400px" controls autoplay muted>
    <source src="/imgs/Fizika/fotonika-gauss-laguerre/gauss-laguerre-video.webm" />
  </video>
  <figcaption>
    Video uporabe <a href="/gauss-laguerre">WebGl programčka</a> za vizualizacijo Gauss-Laguerrovih snopov. Prvo
    prikažemo Gaussov snop, nato pa vijačne oblike, ki jih dobimo za višje rede
    <span class="math inline">\(\ell \neq 0\)</span>
  </figcaption>
</figure>

<h1>Gauss-Laguerrovi snopi</h1>
<h1 class="subtitle">
  <a href="/gauss-laguerre">WebGl programček</a> za vizualizacijo Gauss-Laguerrovih snopov z emisijsko-absorbcijskim
  modelom
</h1>

<p>
  Gauss-Laguerrovi snopi so rešitve obosnega približka valovne enačbe <span
    class="citation"
    data-cites="olenik2022optika"
    >[<a href="#ref-olenik2022optika" role="doc-biblioref">1</a>]</span
  >, <span class="citation" data-cites="wiki-gaussian">[<a href="#ref-wiki-gaussian" role="doc-biblioref">2</a>]</span>.
  Identificiramo jih z celoštevilskima redoma <span class="math inline">\((p, \ell)\)</span>, kjer je
  <span class="math inline">\(p &gt; 0\)</span>.
  <span id="eq:beam" class="math display"
    >\begin{equation}\psi_{p\ell} (r, \varphi, z) = \frac{w_0}{w} \left( \frac{\sqrt{2} r}{w} \right)^{|\ell|}
    L_p^{|\ell|} \left( \frac{2r^2}{w^2} \right) e^{i\ell \varphi} e^{ikr^2 / 2q} e^{-i\eta_{p\ell}(z)},
    \label{eq:beam}\end{equation}</span
  >
  pri čemer je kompleksni krivinski radij <span class="math inline">\(q\)</span> definiran kot
  <span class="math display"
    >\[\frac{1}{q} = \frac{1}{R} + i\frac{2}{kw^2} = \frac{z}{z^2 + z_0^2} + \left( \frac{i}{k w_0^2} \right) \frac{1}{1
    + \frac{z^2}{z_0^2}},\]</span
  >
  kar vključuje tako krivinski radij paraboličnih valovnih front <span class="math inline">\(R\)</span> in širino snopa
  <span class="math inline">\(w\)</span>. Obe količini se spreminjata vzdolž žarka, sicer kot
  <span id="eq:R-and-w" class="math display"
    >\begin{equation}R(z) :=z \left( 1 + \frac{z_0^2}{z^2} \right), \quad w^2(z) :=w_0^2 \left( 1 + \frac{z^2}{z_0^2}
    \right). \label{eq:R-and-w}\end{equation}</span
  >
  Dodatno ima faza snopa še eno bolj homogeno komponento <span class="math inline">\(\eta_{p\ell}\)</span>, ki se ne
  spreminja z cilindričnim radijem <span class="math inline">\(r\)</span>, temveč je z potjo vzdolž osi.
  <span class="math display">\[\eta_{p\ell}(z) = (2p + |\ell| + 1) \arctan \left( \frac{z}{z_0} \right).\]</span>
  Posebej zanimiv pa je v (<a href="#eq:beam" data-reference-type="ref" data-reference="eq:beam">1</a>) faktor
  <span class="math inline">\(e^{i\ell\varphi}\)</span>. Ta predstavlja <span class="math inline">\(\ell\)</span>-krat
  ponovljen potek faze od <span class="math inline">\([0, 2\pi)\)</span>, v ali v obratni urinega kazalci glede na
  predznak <span class="math inline">\(\ell\)</span>. Ta faza je tudi ključni element, ki da snopu tirno vrtilno
  količino <span class="citation" data-cites="wiki-oal">[<a href="#ref-wiki-oal" role="doc-biblioref">3</a>]</span>.
</p>
<figure id="fig:gauss-intensity">
  <div class="center">
    <img src="/imgs/Fizika/fotonika-gauss-laguerre/gauss-intensity.png" width="300px" />
  </div>
  <figcaption>
    Amplitudna slika Gaussovega snopa oz. posebnega primera Gauss-Laguerrovega snopa
    <span class="math inline">\(\psi_{00}\)</span>.
  </figcaption>
</figure>
<h2 id="vizualizacija-snopa">Vizualizacija snopa</h2>
<p>
  Osnovna ideja za vizualizacijo je to, da upodobimo skalarno polje
  <span class="math inline">\(v(\mathbf r) \in \mathbb{R}\)</span> v 3 dimenzijah. V namen tega upodobimo presevno sliko
  (ang. <em>volumetric render</em>), ki posnema procesa emisije in preprosto eksponentno atenuacijo <span
    class="citation"
    data-cites="usher-webgl"
    >[<a href="#ref-usher-webgl" role="doc-biblioref">4</a>]</span
  >. Tako posamezni volumni oddajajo <span class="math inline">\(\propto v(\mathbf{r})\)</span> svetlobe, izsevana
  svetloba pa se pri poti skozi snov proti kameri absorbira. Če pot začne na razdalji
  <span class="math inline">\(s\)</span> od kamere, se pri celotni poti do kamere izsevana svetloba atenuira za faktor
  <span class="math display">\[e^{-\int_0^s v(t) \,\mathrm{d}t}\]</span> Polje opazujemo s točkovno kamero, katere
  piksli pokrijejo nek nabor prostorskih kotov <span class="math inline">\(\Omega\)</span>. Na tej poti je torej
  <span class="math inline">\(v(t) = v(\mathbf{r}(t))\)</span>, pri čemer je žarek
  <span class="math inline">\(\mathbf{r}(t)\)</span> določen s prostorskim kotom. Za žarek iz točkovne kamere pod nekim
  kotom je opažena intenziteta vsota izsevanih in atenuiranim prispevkom vseh volumnov, ki jih žarek seka
  <span class="math display">\[I(\Omega) = \int_0^\infty v(s)\, e^{-\int_0^s v \,\mathrm{d}t} \,\mathrm{d}s.\]</span>
  Numeričen približek takega integrala nam tedaj da sliko <span class="math inline">\(I(\Omega)\)</span>, ki predstavlja
  naše realno skalarno polje v 3 dimenzijah. Po zgledu <span class="citation" data-cites="usher-webgl"
    >[<a href="#ref-usher-webgl" role="doc-biblioref">4</a>]</span
  >
  sem implementiral preprost <a href="https://pompez.net/gauss-laguerre/">WebGL programček</a>, s katero lahko na ta
  način vizualiziramo realna skalarna polja.
</p>
<figure id="fig:gauss-side">
  <div class="center">
    <img src="/imgs/Fizika/fotonika-gauss-laguerre/gauss-intensity-side.png" width="250px" />
    <img src="/imgs/Fizika/fotonika-gauss-laguerre/gauss-phase-side.png" width="250px" />
  </div>
  <figcaption>
    Stranski pogled Gaussovega snopa oz. snopa <span class="math inline">\(\psi_{00}\)</span>. Na levi sliki je
    prikazana amplitudna slika, na desno pa slika z valovnimi frontami. Jasno vidimo, da ukrivljenost front sprva
    narašča, nato pa spet pada.
  </figcaption>
</figure>
<p>
  Gauss-Laguerrovov snop, ki ga želimo vizualizirati, pa je podan z kompleksnim skalarnim poljem. En način za
  vizualizacijo je prikaz same intenzitete polja<a href="#fn1" class="footnote-ref" id="fnref1" role="doc-noteref"
    ><sup>1</sup></a
  >
  <span class="math display">\[\text{amplitudna slika: } v(\mathbf{r}) :=\| \psi_{p\ell} (r, \varphi, z) \|.\]</span>
</p>
<p>
  Na sliki <a href="#fig:gauss-intensity" data-reference-type="ref" data-reference="fig:gauss-intensity">1</a> vidimo v
  perspektivi prikazano amplitudno sliko Gaussovega snopa. Drugi način, ki pa bolj jasno razkrije fazno strukturo snopa,
  pa je prikaz valovnih front. Ideja je, da prikažemo intenziteto, a prikaz omejimo le na eno valovno fronto, torej na
  neko površino konstantne faze <span class="math inline">\(\vartheta_0\)</span>
  <span class="math display"
    >\[\text{valovne fronte: } v(\mathbf{r}) :=\delta(\vartheta - \vartheta_0) \cdot \| \psi_{p\ell} (r, \varphi, z) \|,
    \quad \psi_{p\ell} = \| \psi_{p\ell} \| e^{i\vartheta},\]</span
  >
  pri tem je <span class="math inline">\(\delta(x)\)</span> neka ozka, Dirac-delta funkciji podobna funkcija z
  vrednostjo <span class="math inline">\(1\)</span> pri <span class="math inline">\(x=0\)</span> in vrednostjo
  <span class="math inline">\(0\)</span> pri <span class="math inline">\(|x| \gg 0\)</span>. Za ilustracijo je sliki <a
    href="#fig:gauss-side"
    data-reference-type="ref"
    data-reference="fig:gauss-side"
    >2</a
  >
  razvidna primerjava med amplitudno sliko in sliko valovnih front za Gaussove snop. Dodam naj, da zahtevamo od te
  <span class="math inline">\(\delta(x)\)</span> funkcije periodičnost na <span class="math inline">\(2\pi\)</span>, saj
  je faza tam periodična. Konkretno za našo rabo izberemo
  <span class="math display"
    >\[\begin{aligned} \delta(\vartheta - \vartheta_0) :=1 - \mathop{\mathrm{smoothstep}}\left(0, \sigma,
    \mathop{\mathrm{fract}} \left( \frac{\vartheta - \vartheta_0}{2\pi} \right)\right)&amp; \\ +
    \mathop{\mathrm{smoothstep}}\left(1-\sigma, 1, \mathop{\mathrm{fract}} \left( \frac{\vartheta - \vartheta_0}{2\pi}
    \right)\right),&amp; \end{aligned}\]</span
  >
  pri čemer sta <a href="https://en.wikipedia.org/wiki/Fractional_part">fract</a> in
  <a href="https://en.wikipedia.org/wiki/Smoothstep">smoothstep</a> dve klasični GLSL funkciji.
</p>
<aside id="footnotes" class="footnotes footnotes-end-of-section" role="doc-footnote">
  <hr />
  <ol>
    <li id="fn1">
      <p>
        Takšen prikaz dobimo če v vmesniku izklopimo opcijo "Fazne fronte".<a
          href="#fnref1"
          class="footnote-back"
          role="doc-backlink"
          >↩︎</a
        >
      </p>
    </li>
  </ol>
</aside>
<h2 id="zanimivi-snopi">Zanimivi snopi</h2>
<p>
  Poleg relativno nezanimivega Gaussovega snopa <span class="math inline">\(\psi_{00}\)</span>, si lahko pogledamo drugi
  najpreprostejši Gauss-Laguerrovov snop <span class="math inline">\(\psi_{01}\)</span>. V amplitudni sliki je
  pravzaprav podoben Gaussu, le z ničlo pri <span class="math inline">\(z\)</span>-osi, saj nam to narekuje faktor
  <span class="math inline">\(\left( \frac{\sqrt{2} r}{w} \right)\)</span> v snopu (<a
    href="#eq:beam"
    data-reference-type="ref"
    data-reference="eq:beam"
    >1</a
  >). Povsem drugače pa izgleda oblika valovnih front. Kot vidimo na sliki <a
    href="#fig:p0-ell1"
    data-reference-type="ref"
    data-reference="fig:p0-ell1"
    >3</a
  >, so valovne fronte v obliki vijačnice, z maksimumom pri nekem končnem <span class="math inline">\(r\)</span>. Koliko
  števna je ta vijačnica, je odvisno od <span class="math inline">\(|\ell|\)</span>, smer pa od predznaka
  <span class="math inline">\(\ell\)</span>. To lepo vidimo tudi v prerezih na sliki <a
    href="#fig:front"
    data-reference-type="ref"
    data-reference="fig:front"
    >4</a
  >.
</p>
<figure id="fig:p0-ell1">
  <div class="center">
    <img width="300px" src="/imgs/Fizika/fotonika-gauss-laguerre/p0-ell1-side.png" />
    <img width="300px" src="/imgs/Fizika/fotonika-gauss-laguerre/p0-ell1-spiral.png" />
  </div>
  <figcaption>
    Fazne fronte Gauss-Laguerrovega snopa <span class="math inline">\(\psi_{01}\)</span>. Na levi strani je razvidna
    splošna oblika valovnih front snopa, na desno pa se še lepše vidi značilna vijačnica, ki je bolj opazna, če porežemo
    po absolutni vrednosti manjše dele polja.
  </figcaption>
</figure>
<figure id="fig:front">
  <div class="center">
    <img width="200px" src="/imgs/Fizika/fotonika-gauss-laguerre/front-ell-minus-1.png" />
    <img width="200px" src="/imgs/Fizika/fotonika-gauss-laguerre/front-ell0.png" />
    <img width="200px" src="/imgs/Fizika/fotonika-gauss-laguerre/front-ell1.png" />
    <img width="200px" src="/imgs/Fizika/fotonika-gauss-laguerre/front-ell2.png" />
    <img width="200px" src="/imgs/Fizika/fotonika-gauss-laguerre/front-ell3.png" />
  </div>
  <figcaption>
    Prečni prerezi žarkov <span class="math inline">\(\psi_{0\ell}\)</span> za
    <span class="math inline">\(\ell = -1, 0, 1, 2, 3\)</span>.
  </figcaption>
</figure>
<h1 class="unnumbered" id="bibliography">Viri</h1>
<div id="refs" class="references csl-bib-body" data-entry-spacing="0" role="list">
  <div id="ref-olenik2022optika" class="csl-entry" role="listitem">
    <div class="csl-left-margin">[1]</div>
    <div class="csl-right-inline">
      Čopič in M. Vilfan, <em>Fotonika</em>. v Fizika: zbirka fizikalnih učbenikov in monografij. Fakulteta za
      matematiko in fiziko Univerza v Ljubljani, 2022.
    </div>
  </div>
  <div id="ref-wiki-gaussian" class="csl-entry" role="listitem">
    <div class="csl-left-margin">[2]</div>
    <div class="csl-right-inline">
      Wikipedia contributors,
      <span>„Gaussian beam — <span>Wikipedia</span><span>,</span> The Free Encyclopedia: Laguerre-Gaussian modes“</span
      >. 2024. Dostopno na:
      <a href="https://en.wikipedia.org/w/index.php?title=Gaussian_beam&amp;oldid=1264005971#Laguerre-Gaussian_modes"
        >https://en.wikipedia.org/w/index.php?title=Gaussian_beam&amp;oldid=1264005971#Laguerre-Gaussian_modes</a
      >. [Pridobljeno: 8. januar 2025]
    </div>
  </div>
  <div id="ref-wiki-oal" class="csl-entry" role="listitem">
    <div class="csl-left-margin">[3]</div>
    <div class="csl-right-inline">
      Wikipedia contributors,
      <span>„Orbital angular momentum of light — <span>Wikipedia</span><span>,</span> The Free Encyclopedia"“</span>.
      2024. Dostopno na:
      <a href="https://en.wikipedia.org/w/index.php?title=Orbital_angular_momentum_of_light&amp;oldid=1258318355"
        >https://en.wikipedia.org/w/index.php?title=Orbital_angular_momentum_of_light&amp;oldid=1258318355</a
      >. [Pridobljeno: 8. januar 2025]
    </div>
  </div>
  <div id="ref-usher-webgl" class="csl-entry" role="listitem">
    <div class="csl-left-margin">[4]</div>
    <div class="csl-right-inline">
      W. Usher, <span>„Volume Rendering with WebGL“</span>. 2019. Dostopno na:
      <a href="https://www.willusher.io/webgl/2019/01/13/volume-rendering-with-webgl/"
        >https://www.willusher.io/webgl/2019/01/13/volume-rendering-with-webgl/</a
      >. [Pridobljeno: 8. januar 2025]
    </div>
  </div>
</div>
