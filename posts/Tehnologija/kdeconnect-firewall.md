If you use a firewall, you need to open UDP and TCP ports 1714 through 1764.

On EndeavorOS, firewalld is installed by default, and by `systemctl status firewalld` I could see that it is running.

```
sudo firewall-cmd --permanent --add-port=1714-1764/udp
sudo firewall-cmd --reload
sudo firewall-cmd --list-ports
```

And then reload on the app or on the PC

```
kdeconnect-cli --refresh
kdeconnect-cli -l
```
