---
author: pompež
date: 10. april 2024
title: Varnostne kopije z Borg
keywords: [backup, Borg, BorgBackup, varnostna kopija]
---

# Zakaj?

Ker ti lahko v trenutnu nepozornosti varnostna kopija prepreči sicer zelo bolečo izkušnjo. Če nisi prepričan, še nisi imel zadostnega števila zadostno bolečih izkušenj.

# Diski in fstab

Poleg glavnega diska imam na svojem domačem serverju parkirana še dva identična 2TB HDD diska, katerih namen je hramba večjih datotek in backup. Zelo preprosto sta mount-ana preko njunih UUID-jev. V ta namen dodamo v `/etc/fstab` sledeča vnosa:

```
# Primary and secondary (backup) HDDs
UUID=88bdb08d-8f56-4920-8384-53113eb19ba3    /media/primary    ext4    defaults,rw,auto_da_alloc    0    2
UUID=5a83d8c6-738a-497b-b17c-c939b354348c    /media/secondary  ext4    defaults,rw,auto_da_alloc    0    2
```

Potrebne UUID-je lahko izvemo z uporabo `lsblk -f`, po modifikaciji lahko `fstab` spremembe uveljavimo z `systemctl daemon-reload && sudo mount -a`, avtomatsko pa se to izvede tudi ob vsakem zagonu.

[Footnote] Preverimo tudi, da sta /media/primary in /media/secondary direktoriji v primernem lastništvu, drugače bomo imeli kasneje probleme pri pisanju vanje.

```
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
sda           8:0    0   1,8T  0 disk
└─sda1        8:1    0   1,8T  0 part /media/primary
sdb           8:16   0   1,8T  0 disk
└─sdb1        8:17   0   1,8T  0 part /media/secondary
```

# Varnostne kopije z BorgBackup
