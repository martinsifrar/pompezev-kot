post_key = nil -- Key of the post, e.g. "Fizika/maf2-zakljucna"

 -- Set key of the post, passed in Makefile rule for *.editable.html
function Meta(m)
    post_key = m.post_key
    return m
end

-- First return is basename w/o extension after the last "/"
-- Second return is filetype extension, e.g. "pdf" in "folder/a.pdf"
function split_name(str)
    local basename = string.match(str, "([^/]+)$")
    local basename_wo_ext, ext = string.match(basename, "(.+)%.([^.]*)$")
    if ext == nil then
        return basename, nil
    else 
        return basename_wo_ext, ext
    end
end

-- Set correct src for <img> tags based on convention
function Image(el)
    local fig_basename, _ = split_name(el.src)
    local dir = "/imgs"
    local png_filename = string.format("%s/%s/%s.png", dir, post_key, fig_basename)

    el.src = png_filename
    return el
end

eq_idx = 0
eq_labels = {}

-- Fix default [\ \] to ams environments for labeled equations
function Math(m)
    if m.mathtype == "InlineMath" then
        return m
    end

    local label = string.match(m.text, "\\label{(eq:[^}]+)}")

    if label ~= nil then
        eq_idx = eq_idx + 1
        eq_labels["#" .. label] = eq_idx
        local html = string.format(
          '<span id="%s" class="math display">\\begin{equation}%s\\end{equation}</span>',
          label,
          m.text
        )
        return pandoc.RawInline("html", html)
    end
    return m
end

-- Fix link content to equation number
function Link2Eq(m)
    local idx = eq_labels[m.target]
    if idx ~= nil then
        m.content = tostring(idx)
    end
    return m
end

-- Run citeproc
function Pandoc(doc)
  return pandoc.utils.citeproc(doc)
end

a = nil

-- Add citations links and fix output
-- Currently doesnt handle multiple citations
-- (split them in the TeX source file)
function Cite(m)  
  local cite_text = pandoc.utils.stringify(m.content)
  local number, rest = string.match(cite_text, "%[(%d+)(.*)%]")

  if number ~= nil and rest ~= nil then
      local numberHref = pandoc.Link(number, "#ref-" .. m.citations[1].id)
      m.content = { "[", numberHref, rest, "]"}
  end
  -- m.content = number .. "a" -- Weird cause it fails without nil check
  return m
end

bibentry_txt = nil

return {
    { Meta = Meta },
    { Image = Image },
    { Math = Math },
    { Link = Link2Eq },
    { Pandoc = Pandoc },
    { Cite = Cite },
}
