import fs from 'fs/promises';
import mustache from 'mustache';

const bodyFilename = process.argv[2];
const headerIncludeFile = process.argv[3];

const body = bodyFilename
    ? await fs.readFile(bodyFilename, "utf8")
    : ( console.error("(ERROR) Arg $1 undefined."), process.exit(1) );
const headerInclude = headerIncludeFile
    ? await fs.readFile(headerIncludeFile, "utf8")
    : "";
const template = await fs.readFile("./template.html", "utf8");

const titleRegex = /<h1>(.*)<\/h1>/;
var m = body.match(titleRegex);
var title;
if (!m) {
    console.warn("(WARN) No <h1> tag found for title.");
    title = "Pompežev kot" // Fallback
} else
    title = m[1]

const subtitleRegex = /<h1 class="subtitle">(.*)<\/h1>/;
m = body.match(subtitleRegex);
var subtitle;
if (!m) {
    console.warn('(WARN) No <h1 class="subtitle"> tag found.');
    subtitle = ""
} else
    subtitle = m[1]

const constants = {
    title,
    subtitle,
    headerInclude: headerInclude.trimEnd(),
    body: body.trimEnd()
};

const output = mustache.render(template, constants);
console.log(output);
