#!/usr/bin/sh
# Dependencies:
#   pandoc,
#   make,
#   node:
#     mustache,
#     (optional) prettier,
#     (optional) live-server,
#   (optional) trimage (for image optimization)

POSTS_DIR=posts/
POSTS_OUTDIR=static/

all: static
.PHONY: dev deploy format

dev: static
	npm run dev

deploy: static
	./deploy-copy.A
	# ./deploy-init.B # Just the first time

format:
	npx prettier --write posts/**/*.html
	
posts/%.editable.html: posts/%.tex
	pandoc -f latex -t html $< \
		--template template.editable.html \
		--lua-filter fizika-filters.lua \
		--shift-heading-level-by=1 \
		--metadata post_key="$*" \
		--metadata reference-section-title="Viri" \
		--bibliography="posts/$*.bib" \
		--reference-location section \
		--csl=ieee.csl \
		--mathjax \
		> $@
	npx prettier --write $@

# Editable HTML files in /posts
# 	make posts/Fizika/maf2-zakljucna.editable.html
# 	make posts/Fizika/fot1-gauss-laguerre.editable.html
#
#   make posts/Fizika/mod1-01-voznja-skozi-semafor.editable.html
#   make posts/Fizika/mod1-02-linearno-programiranje.editable.html
#   make posts/Fizika/mod1-03-optimizacija.editable.html
#   make posts/Fizika/mod1-04-populacije.editable.html
#   make posts/Fizika/mod1-05-kemijske-reakcije.editable.html
#   make posts/Fizika/mod1-06-fitanje.editable.html
#   make posts/Fizika/mod1-07-mc.editable.html
#   make posts/Fizika/mod1-08-mcmc.editable.html
#   make posts/Fizika/mod1-09-stohastic-de.editable.html

static/%.html: posts/%.editable.html static/imgs/% template.mjs template.html
	node template.mjs $< > $@
	npx prettier --write $@

static/Fizika/%.html: posts/Fizika/%.editable.html static/imgs/Fizika/% template.mjs template.html mathjax-include.html
	node template.mjs $< mathjax-include.html > $@
	npx prettier --write $@

static: \
	static/index.html \
	static/RPGs/Incident-v-Herfovem-Braniku.html \
	static/Fizika/maf2-zakljucna.html \
	static/Fizika/fot1-gauss-laguerre.html \
	\
	static/Fizika/mod1-01-voznja-skozi-semafor.html \
	static/Fizika/mod1-02-linearno-programiranje.html \
	static/Fizika/mod1-03-optimizacija.html \
	static/Fizika/mod1-04-populacije.html \
	# static/Fizika/mod1-05-kemijske-reakcije.html \
	# static/Fizika/mod1-06-fitanje.html \
	# static/Fizika/mod1-07-mc.html \
	# static/Fizika/mod1-08-mcmc.html \
	# static/Fizika/mod1-09-stohastic-de.html \

include Makefile.imgs
include Makefile.srcs
